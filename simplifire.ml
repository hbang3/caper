(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Simplifier for pcap expressions.
*)

open Pcap_syntax
open Pcap_syntax_aux

(*FIXME move somewhere generic*)
(*Iterate a function until a fixpoint*)
let rec trampoline ?(limit : int = !Config.max_trampoline_iterate) (label : string) (f : 'a -> 'a) (x : 'a) : 'a =
  if limit = 0 then
    begin
      Aux.diag_output ("trampoline exceeded limit: " ^ label);
      x
    end
  else
    let y = f x in
    if y = x then y
    else trampoline ~limit:(limit - 1) label f y

let rec trampoline_until_pred ?(limit : int = !Config.max_trampoline_iterate) (label : string) (pred : 'a -> bool) (f : 'a -> 'a) (x : 'a) : 'a =
  if limit = 0 then
    begin
      Aux.diag_output ("trampoline_until_pred exceeded limit: " ^ label);
      x
    end
  else
    if pred x then x
    else
      let y = f x in
      if pred y || y = x then y
      else trampoline_until_pred ~limit:(limit - 1) label pred f y

let rec simplify_pcap_singleton_connectives (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _ -> pe
  | Not pe -> Not (simplify_pcap_singleton_connectives pe)
  | And pes ->
      begin
        match List.map simplify_pcap_singleton_connectives pes with
        | [] -> True
        | [pe] -> pe
        | pes -> And pes
      end
  | Or pes ->
      begin
        match List.map simplify_pcap_singleton_connectives pes with
        | [] -> False
        | [pe] -> pe
        | pes -> Or pes
      end

let rec flatten_and (pe : pcap_expression) (acc : pcap_expression list) : pcap_expression list =
  match pe with
  | And pes -> List.fold_right flatten_and pes acc
  | _ -> flatten' pe :: acc
and flatten_or (pe : pcap_expression) (acc : pcap_expression list) : pcap_expression list =
  match pe with
  | Or pes -> List.fold_right flatten_or pes acc
  | _ -> flatten' pe :: acc
and flatten' (pe : pcap_expression) : pcap_expression =
  match pe with
  | And [pe']
  | Or [pe'] -> flatten' pe'
  | And pes ->
      And (List.fold_right flatten_and pes [])
      |> simplify_pcap_singleton_connectives
  | Or pes ->
      Or (List.fold_right flatten_or pes [])
      |> simplify_pcap_singleton_connectives
  | Not pe' -> Not (flatten' pe')
  | _ -> pe

let rec flatten_plus (e : expr) (acc : expr list) : expr list =
  match e with
  | Plus es -> List.fold_right flatten_plus es acc
  | _ -> flatten_expr' e :: acc
and flatten_times (e : expr) (acc : expr list) : expr list =
  match e with
  | Times es -> List.fold_right flatten_times es acc
  | _ -> flatten_expr' e :: acc
and flatten_band (e : expr) (acc : expr list) : expr list =
  match e with
  | Times es -> List.fold_right flatten_band es acc
  | _ -> flatten_expr' e :: acc
and flatten_bor (e : expr) (acc : expr list) : expr list =
  match e with
  | Times es -> List.fold_right flatten_bor es acc
  | _ -> flatten_expr' e :: acc
and flatten_expr' (e : expr) : expr =
  match e with
  | Plus [e']
  | Times [e']
  | Binary_And [e']
  | Binary_Or [e'] -> flatten_expr' e'
  | Plus es -> Plus (List.fold_right flatten_plus es [])
  | Times es -> Times (List.fold_right flatten_times es [])
  | Binary_And es -> Binary_And (List.fold_right flatten_band es [])
  | Binary_Or es -> Binary_Or (List.fold_right flatten_bor es [])
  | _ -> e

let value_of (e : expr) : int option =
  match e with
  | Nat_Literal i -> Some i
  | Hex_Literal h ->
    let i = Scanf.sscanf h "%u" (fun x -> x) in
    Some i
  | _ -> None

(*FIXME flatten first,
then order according to weight*)
let rec simp_expr (e : expr) : expr =
  match e with
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> e

  | Plus [e']
  | Times [e']
  | Binary_And [e']
  | Binary_Or [e'] -> simp_expr e'
  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Shift_Right (e1, e2)
  | Shift_Left (e1, e2) ->
    let semantic =
      match e with
      | Minus (_, _) ->
        fun x y -> x - y
      | Quotient (_, _) ->
        fun x y -> x / y
      | Shift_Right (_, _) ->
        fun x y -> x lsr y
      | Shift_Left (_, _) ->
        fun x y -> x lsl y
      | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "Impossible")
    in
      begin
        match value_of e1, value_of e2 with
        | Some i1, Some i2 ->
          Nat_Literal (semantic i1 i2)
        | _, _ -> e
      end
  | Packet_Access (proto_name, e, no_bytes_opt) ->
    Packet_Access (proto_name, simp_expr e, no_bytes_opt)
  | _ -> failwith (Aux.error_output_prefix ^ ": simp_expr: " ^ "Unsupported expression")

let generic_simp_rel_expr (rel_op : expr -> expr -> rel_expr)
 (semantic : int -> int -> pcap_expression) (e1 : expr) (e2 : expr) : pcap_expression =
  let e1' = simp_expr e1 in
  let e2' = simp_expr e2 in
  match (value_of e1', value_of e2') with
  | Some i1, Some i2 -> semantic i1 i2
  | _, _ -> Atom (rel_op e1' e2')

let simp_rel_expr : rel_expr -> pcap_expression = function
  | Gt (e1, e2) ->
(*
    let e1' = simp_expr e1 in
    let e2' = simp_expr e2 in
    match (value_of e1', value_of e2') with
    | Some i1, Some i2 ->
      if i1 > i2 then True else False
    | _, _ -> Atom (Gt (e1', e2'))
*)
    generic_simp_rel_expr (fun e1 e2 -> Gt (e1, e2))
     (fun i1 i2 -> if i1 > i2 then True else False) e1 e2
  | Lt (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Lt (e1, e2))
     (fun i1 i2 -> if i1 < i2 then True else False) e1 e2
  | Geq (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Geq (e1, e2))
     (fun i1 i2 -> if i1 >= i2 then True else False) e1 e2
  | Leq (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Leq (e1, e2))
     (fun i1 i2 -> if i1 <= i2 then True else False) e1 e2
  | Eq (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Eq (e1, e2))
     (fun i1 i2 -> if i1 = i2 then True else False) e1 e2
  | Neq (e1, e2) ->
    generic_simp_rel_expr (fun e1 e2 -> Neq (e1, e2))
     (fun i1 i2 -> if i1 <> i2 then True else False) e1 e2

(* FIXME partly overlaps with other functions, e.g., simplify_pcap_singleton_connectives*)
let rec simplify_pcap' (pe : pcap_expression) : pcap_expression =
  match pe with
  | And [] -> True
  | Or [] -> False
  | Primitive _ -> pe (*NOTE currently we don't simplify primitives*)
(*  | Atom _ -> pe (*FIXME this might need to expand -- e.g., to require that "ip"*)*)
(*  | Atom re -> simp_rel_expr re*)
  | Atom _ -> pe(*FIXME currently don't simplify inside atoms*)

  | Not (Atom (Gt (e1, e2))) ->
      Atom (Leq (e1, e2))(*FIXME currently don't simplify inside atoms*)
  | Not (Atom (Lt (e1, e2))) ->
      Atom (Geq (e1, e2))(*FIXME currently don't simplify inside atoms*)
  | Not (Atom (Geq (e1, e2))) ->
      Atom (Lt (e1, e2))(*FIXME currently don't simplify inside atoms*)
  | Not (Atom (Leq (e1, e2))) ->
      Atom (Gt (e1, e2))(*FIXME currently don't simplify inside atoms*)
  | Not (Atom (Eq (e1, e2))) ->
      Atom (Neq (e1, e2))(*FIXME currently don't simplify inside atoms*)
  | Not (Atom (Neq (e1, e2))) ->
      Atom (Eq (e1, e2))(*FIXME currently don't simplify inside atoms*)

  | And pes
  | Or pes ->
      begin
      let pes' = List.map simplify_pcap' pes in
      let recreate_connective pes =
        begin
          match pe with
          | And _ -> And pes
          | Or _ -> Or pes
          | _ -> failwith "Impossible"
        end in
      match pes' with
      | [] -> failwith "Impossible"
      | [pe] -> pe
      | _ -> recreate_connective pes'
      end
  | Not (Not pe) -> simplify_pcap' pe
  | Not True -> False
  | Not False -> True
  | Not pe -> Not (simplify_pcap' pe)
  | True
  | False -> pe

let flatten (pe : pcap_expression) : pcap_expression =
  trampoline "flatten" flatten' pe
(*
let simplify_pcap (pe : pcap_expression) : pcap_expression =
  trampoline simplify_pcap' pe
*)

(* Simplify conjunctions and disjunctions, filtering out Trues and False as
   sensible, and possibly reducing the subformula to True or False*)
let rec simplify_pcap_connectives (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _ -> pe
  | Not pe -> Not (simplify_pcap_connectives pe)
  | And pes ->
      let falsified = ref false in
      let pes' = List.fold_right (fun pe' acc ->
        if !falsified then []
        else if pe' = True then
          begin
            Aux.diag_output ("INFO: simplify_pcap_connectives : " ^
             Pcap_syntax_aux.string_of_pcap_expression pe ^
             " filtering " ^
             Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
            acc
          end
        else if pe' = False then
          begin
            falsified := true;
            Aux.diag_output ("INFO: simplify_pcap_connectives : " ^
             Pcap_syntax_aux.string_of_pcap_expression pe ^
             " because of " ^
             Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
            []
          end
        else (simplify_pcap_connectives pe') :: acc) pes [] in
      if not !falsified then
        And pes'
      else False
  | Or pes ->
      let verified = ref false in
      let pes' = List.fold_right (fun pe' acc ->
        if !verified then []
        else if pe' = False then
          begin
            Aux.diag_output ("INFO: simplify_pcap_connectives : " ^
             Pcap_syntax_aux.string_of_pcap_expression pe ^
             " filtering " ^
             Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
            acc
          end
        else if pe' = True then
          begin
            verified := true;
            Aux.diag_output ("INFO: simplify_pcap_connectives : " ^
             Pcap_syntax_aux.string_of_pcap_expression pe ^
             " because of " ^
             Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
            []
          end
        else (simplify_pcap_connectives pe') :: acc) pes [] in
      if not !verified then
        Or pes'
      else True

(*Find contradicting subexpressions and simplify any affected connectives.
  For example, "ip && ! ip" is simplified to False,
  as is "ip6 && ip".*)
(* FIXME pass an accumulation of conjoined facts *)
let rec simplify_pcap_contradiction (pe : pcap_expression) : pcap_expression =
  match pe with
  | Primitive ((Some proto, _, Some Host), IPv4_Address _)
  | Primitive ((Some proto, _, Some Net), IPv4_Network _)
  | Primitive ((Some proto, _, Some Net), IPv4_Network_Masked _) ->
      if proto = Ip6 then False else pe
  | Primitive ((Some proto, _, Some Host), IPv6_Address _)
  | Primitive ((Some proto, _, Some Net), IPv6_Network _) ->
      if proto = Ip || proto = Rarp || proto = Arp then False else pe
  | True
  | False
  | Primitive _
  | Atom _ -> pe
  | Not pe -> Not (simplify_pcap_contradiction pe)
  | And pes ->
      let falsified = ref false in
      let pes' = List.fold_right (fun pe acc ->
        let _(*acc'*) = List.fold_right (fun pe' acc ->
          if !falsified then []
          else
            let newly_falsified =
              match pe' with
              | Atom re ->
                  (*FIXME could check for inconsistent relations*)
                  begin
                    match pe with
                    | Primitive ((Some proto, None, Some Proto), Escaped_String sub_proto) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          (proto = Tcp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Sctp protos)) ||
                          (proto = Udp && (Names.ProtoSet.mem Tcp protos || Names.ProtoSet.mem Sctp protos)) ||
                          (proto = Sctp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Tcp protos)) ||
                          (proto = Ip && Names.ProtoSet.mem Ip6 protos) ||
                          (proto = Ip6 && Names.ProtoSet.mem Ip protos) ||
                          (string_of_proto proto = sub_proto) ||
                          (sub_proto = string_of_proto Tcp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Sctp protos)) ||
                          (sub_proto = string_of_proto Udp && (Names.ProtoSet.mem Tcp protos || Names.ProtoSet.mem Sctp protos)) ||
                          (sub_proto = string_of_proto Sctp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Tcp protos)) ||
                          (sub_proto = string_of_proto Ip && Names.ProtoSet.mem Ip6 protos) ||
                          (sub_proto = string_of_proto Ip6 && Names.ProtoSet.mem Ip protos)
                        end
                    | Primitive ((Some proto, _, Some Host), IPv4_Address _)
                    | Primitive ((Some proto, _, Some Net), IPv4_Network _)
                    | Primitive ((Some proto, _, Some Net), IPv4_Network_Masked _) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          proto = Ip6 || Names.ProtoSet.mem Ip6 protos
                        end
                    | Primitive ((_, _, Some Net), IPv4_Network _)
                    | Primitive ((_, _, Some Net), IPv4_Network_Masked _)
                    | Primitive ((_, _, Some Host), IPv4_Address _) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          Names.ProtoSet.mem Ip6 protos
                        end
                    | Primitive ((Some proto, _, Some Host), IPv6_Address _)
                    | Primitive ((Some proto, _, Some Net), IPv6_Network _) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          proto = Ip || Names.ProtoSet.mem Ip protos ||
                          proto = Arp || Names.ProtoSet.mem Arp protos ||
                          proto = Rarp || Names.ProtoSet.mem Rarp protos
                        end
                    | Primitive ((_, _, Some Net), IPv6_Network _)
                    | Primitive ((_, _, Some Host), IPv6_Address _) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          Names.ProtoSet.mem Ip protos ||
                          Names.ProtoSet.mem Arp protos ||
                          Names.ProtoSet.mem Rarp protos
                        end
                    | _ -> false
                  end
              | True
              | False
              | And _
              | Or _ -> pe = Not pe'
              | Not pe'' -> pe = pe''
              | Primitive ((Some proto', None, Some Proto), Escaped_String sub_proto') ->
                  begin
                    match pe with
                    | Primitive ((Some proto, None, Some Proto), Escaped_String sub_proto) ->
                        (proto = proto' && sub_proto <> sub_proto') ||
                        (proto = Ip && proto' = Ip6) || (proto = Ip6 && proto' = Ip)
                    | Primitive ((Some proto, _, _), _) ->
                        (proto = Ip && proto' = Ip6) || (proto = Ip6 && proto' = Ip)
                    | _ -> false
                  end
              | Primitive ((Some proto', _, _), _) ->
                  begin
                    match pe with
                    | Primitive ((Some proto, _, _), _) ->
                        (proto = Ip && proto' = Ip6) || (proto = Ip6 && proto' = Ip)
                    | _ -> false
                  end
              | _ -> false (*FIXME might be too weak*)
            in if newly_falsified then
              begin
                falsified := true;
                Aux.diag_output ("INFO: simplify_pcap_contradiction : " ^
                 Pcap_syntax_aux.string_of_pcap_expression pe ^
                 " and " ^
                 Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
                []
              end
            else pe' :: acc
          ) acc [] in
        if not !falsified then
          pe :: acc
        else []
        ) pes [] in
      if !falsified then False
      else And (List.map simplify_pcap_contradiction pes')
  | Or pes ->
      (*A disjunction can't itself be contradictory (and we check for empty
        disjunctions elsewhere), so we just pass on the processing to subformulas.*)
      Or (List.map simplify_pcap_contradiction pes)

let rec distribute (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _ -> pe

(*
  (*Normalise*) -- FIXME let other functions handle this
  | Not (And pes) -> Or (List.map (fun pe -> Not pe) pes)
  | Not (Or pes) -> And (List.map (fun pe -> Not pe) pes)
  | Not (Not pe') -> pe'
*)

  | Not pe' -> Not (distribute pe')

  (*FIXME make more aggressive rather than rely on specific ordering, as being done currently?*)
  | And (pe :: Or pes :: rest)
  | And (Or pes :: pe :: rest) ->
      (*And ([Or (List.map (fun pe' -> And [pe; pe']) pes)] @ rest)*)
      begin
      let pe_distributed = distribute pe in
      let internal_disjunction = Or (List.map (fun pe' -> And [pe_distributed; distribute pe']) pes) in
      match rest with
      | [] -> internal_disjunction
      | _ -> And (internal_disjunction :: (List.map distribute rest))
      end

  | And pes -> And (List.map distribute pes)
  | Or pes -> Or (List.map distribute pes)

let rec undistribute_forall (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _ -> pe
  | Not pe -> Not (undistribute_forall pe)
  | Or (And pes :: rest) ->
      let conjuncts_present = List.map (fun pe ->
        let present_in_all_conjuncts =
          List.fold_right (fun pe' acc ->
            if not acc then false
            else
              match pe' with
              | And pes'' ->
                  List.fold_right (fun pe' acc' ->
                    if acc' then true
                    else if pe = pe' then true
                    else false) pes'' false
              | _ -> false) rest true in
          (pe, present_in_all_conjuncts)
        ) pes
        |> (fun common_conjuncts ->
            List.fold_right (fun pe_b acc ->
              match pe_b with
              | (pe, true) -> pe :: acc
              | _ -> acc) common_conjuncts []) in
      let pes_minus_common_set pes =
        List.fold_right (fun pe acc ->
          let pe_is_common =
            List.fold_right (fun pe' acc' ->
               if acc' then true
               else if pe = pe' then true
               else false) conjuncts_present false
          in if pe_is_common then acc else pe :: acc) pes []
      in if [] = conjuncts_present then Or (undistribute_forall (And pes) :: List.map undistribute_forall rest)
      else
        let avoid_singleton_conjunction pes =
          let pes' = pes_minus_common_set pes in
          if List.length pes' = 1 then List.hd pes' else And pes' in
        let rest' = List.map (fun pe ->
          match pe with
          | And pes -> avoid_singleton_conjunction pes
          | _ -> failwith "Impossible") rest in
        let first_term = avoid_singleton_conjunction pes in
        And ((Or (first_term :: rest')) :: conjuncts_present)
  | And pes -> And (List.map undistribute_forall pes)
  | Or pes -> Or (List.map undistribute_forall pes)

module OrderedPE =
  struct
    type t = pcap_expression
    let compare pe1 pe2 =
      String.compare (Pcap_syntax_aux.string_of_pcap_expression pe1)
       (Pcap_syntax_aux.string_of_pcap_expression pe2)
  end
module PEMap = Map.Make(OrderedPE)
let string_of_pemap pemap : string =
  PEMap.fold (fun k v (i, s) ->
    (i + 1, s ^ string_of_int i ^ ": v=" ^ string_of_int v ^ " k=" ^ Pcap_syntax_aux.string_of_pcap_expression k ^ "\n")) pemap (0, "")
  |> snd

(*
Pick a most-frequently-occurring subexpression that's in the first level of a disjunction .
Changes "(top || (.. && top && ..)  || ...)" to "top && (.. || ..) || .."
*)
let rec undistribute_some (pe : pcap_expression) : pcap_expression = (*FIXME this code can be made clearer*)
  Aux.diag_output ("undistribute_some pe = " ^ Pcap_syntax_aux.string_of_pcap_expression ~relaxed:true pe ^ "\n");
  match pe with
  | True
  | False
  | Primitive _
  | Atom _ -> pe

  | And [] -> True
  | Or [] -> False

  | Not pe -> Not (undistribute_some pe)
  | And pes -> And (List.map undistribute_some pes)
  | Or orig_pes ->
      let pes = List.map undistribute_some orig_pes in
      let mapping, _, updated_pes =
        List.fold_right (fun pe (acc, viable, updated_pes) ->
         if not viable then (acc, viable, pe :: updated_pes)
         else
           match pe with
           | True
           | False -> (acc, viable, pe :: updated_pes) (*FIXME have this match the case for constants as below, under And?*)
           | Or _ -> failwith "(Or) undistribute_some: should not encounter nested connectives -- formulas should have been flattened."
           | Primitive _
           | Atom _
           | Not _ ->
               let acc' =
                 PEMap.update pe (fun v ->
                  match v with
                  | None -> Some 1
                  | Some n -> Some (n + 1)) acc
               in (acc', viable, pe :: updated_pes)
           | And pes' ->
               let (acc', viable', updated_pes') =
                 List.fold_right (fun pe (acc, viable, updated_pes') ->
                  if not viable then (acc, viable, pe :: updated_pes')
                  else
                    match pe with
                    | True
                    | False -> failwith "undistribute_some: constants should have been removed by now."
                    | And _ -> failwith ("(And) undistribute_some: should not encounter nested connectives -- formulas should have been flattened." ^
                    "Encountered '" ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "'.")
                    | Primitive _
                    | Atom _
                    | Not _ ->
                        let acc' =
                          PEMap.update pe (fun v ->
                           match v with
                           | None -> Some 1
                           | Some n -> Some (n + 1)) acc
                        in (acc', viable, pe :: updated_pes')
                    | Or pes'' -> (acc, false, (Or (List.map undistribute_some pes'')) :: updated_pes') (*FIXME why viable=false; and is there value to consider hoisting disjunctions?*)
                 ) (List.map undistribute_some pes') (acc, viable, [])
               in (acc', viable', (And updated_pes') :: updated_pes)
        ) pes (PEMap.empty, true, []) in
      begin
        let (k, v) = PEMap.fold (fun k v ((_, acc_v) as acc) ->
          if v > acc_v then (k, v) else acc) mapping (False, 0) in
        if v <= 1 then
          begin
          Aux.diag_output "    undistribute_some returning pe\n";
          Or updated_pes
          end
        else
          begin
          Aux.diag_output ("    undistribute_some going with (v=" ^ string_of_int v ^ ") pe=" ^ Pcap_syntax_aux.string_of_pcap_expression ~relaxed:true k ^ "\n");
          let pes_weaker, pes_rest =
            List.fold_right (fun pe ((pes_weaker, pes_rest) as acc) ->
              if pe = k then acc
              else match pe with
              | And and_pes ->
                  let and_pes' =
                    List.fold_right (fun pe acc ->
                      if pe = k then acc
                      else pe :: acc) and_pes []
                  in if List.length and_pes <> List.length and_pes'
                    then (And and_pes' :: pes_weaker, pes_rest)
                    else (pes_weaker, And and_pes :: pes_rest)
              | _ -> (pes_weaker, pe :: pes_rest)) updated_pes ([], [])
          in Or ([And [k; Or pes_weaker]] @ List.map undistribute_some pes_rest)
          end
      end

(*
Like undistribute_some but targets conjunctions of disjunctions.
Changes "((a1 || top || aN) && (b1 || top || bN)  && ...)" to "(top || ((a1 || .. || aN) && (b1 || .. || bN))) && ..."
*)
let rec undistribute_some_conj (pe : pcap_expression) : pcap_expression = (*FIXME this code can be made clearer*)
  Aux.diag_output ("undistribute_some_conj pe = " ^ Pcap_syntax_aux.string_of_pcap_expression ~relaxed:true pe ^ "\n");
  match pe with
  | True
  | False
  | Primitive _
  | Atom _ -> pe

  | And [] -> True
  | Or [] -> False

  | Not pe -> Not (undistribute_some_conj pe)
  | Or pes -> Or (List.map undistribute_some_conj pes)
  | And orig_pes ->
      let pes = List.map undistribute_some_conj orig_pes in
      let mapping, updated_pes =
        List.fold_right (fun pe (acc, updated_pes) ->
         match pe with
         | True
         | False -> (acc, pe :: updated_pes) (*FIXME could be more conservative: failwith "undistribute_some_conj: constants should have been removed by now."*)
         | And _ -> failwith "(And) undistribute_some_conj: should not encounter nested connectives -- formulas should have been flattened."
         | Primitive _
         | Atom _
         | Not _ ->
             let acc' =
               PEMap.update pe (fun v ->
                match v with
                | None -> Some 1
                | Some n -> Some (n + 1)) acc
             in (acc', pe :: updated_pes)
         | Or pes' ->
             let (acc', updated_pes') =
               List.fold_right (fun pe (acc, updated_pes') ->
                match pe with
                | True
                | False -> (acc, pe :: updated_pes') (*FIXME could be more conservative: failwith "undistribute_some_conj: constants should have been removed by now."*)
                | Or _ -> failwith ("(Or) undistribute_some_conj: should not encounter nested connectives -- formulas should have been flattened." ^
                "Encountered '" ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "'.")
                | Primitive _
                | Atom _
                | Not _ -> (*FIXME apply undistribute_some_conj to subformula*)
                    let acc' =
                      PEMap.update pe (fun v ->
                       match v with
                       | None -> Some 1
                       | Some n -> Some (n + 1)) acc
                    in (acc', pe :: updated_pes')
                | And pes'' -> (acc, (And (List.map undistribute_some_conj pes'')) :: updated_pes') (*FIXME allow this to be hoisted*)
               ) (List.map undistribute_some_conj pes') (acc, [])
             in (acc', (Or updated_pes') :: updated_pes)
        ) pes (PEMap.empty, []) in
      begin
        let (k, v) = PEMap.fold (fun k v ((_, acc_v) as acc) ->
          if v > acc_v then (k, v) else acc) mapping (False, 0) in
        if v <= 1 then
          begin
          Aux.diag_output "    undistribute_some_conj returning pe\n";
          And updated_pes
          end
        else
          begin
          Aux.diag_output ("    undistribute_some_conj going with (v=" ^ string_of_int v ^ ") pe=" ^ Pcap_syntax_aux.string_of_pcap_expression ~relaxed:true k ^ "\n");
          let pes_weaker, pes_rest =
            List.fold_right (fun pe (pes_weaker, pes_rest) ->
              match pe with
              | Or or_pes ->
                  let or_pes' =
                    List.fold_right (fun pe acc ->
                      if pe = k then acc
                      else pe :: acc) or_pes []
                  in if List.length or_pes <> List.length or_pes'
                    then (Or or_pes' :: pes_weaker, pes_rest)
                    else (pes_weaker, Or or_pes :: pes_rest)
              | _ -> (pes_weaker, pe :: pes_rest)) updated_pes ([], [])
          in And (Or [k; And pes_weaker] :: pes_rest)
          end
      end

let distribute_if_instructed (pe : pcap_expression) : pcap_expression =
  if !Config.not_undistribute then pe
  else pe
    |> distribute
    |> Aux.diag_output_id (fun pe -> "INFO: distribute: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")


let is_literal (pe : pcap_expression) : bool =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _
  | Not (Primitive _)
  | Not (Atom _) -> true
  | _ -> false

let is_clause (pe : pcap_expression) : bool =
  match pe with
  | Or pes ->
      assert (pes <> []);
      (*pes must consist entirely of literals*)
      List.fold_right (fun pe acc ->
        acc && is_literal pe) pes true
  | _ -> false

(*NOTE it's assumed that the formula has been simplified -- e.g., double negations are removed, etc*)
let is_clausal (pe : pcap_expression) : bool =
  Aux.diag_output ("INFO: is_clausal: " ^ Pcap_syntax_aux.string_of_pcap_expression ~relaxed:true pe ^ "\n");
  match pe with
  | Or _ -> is_clause pe
  | And pes ->
      begin
      assert (pes <> []);
      (*pes must consist entirely of disjunctions or literals*)
      List.fold_right (fun pe acc ->
        acc &&
         match pe with
         | Or _ -> is_clause pe
         | _ -> is_literal pe) pes true
      end
  | _ -> is_literal pe

let rec clausify (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _
  | Not (Primitive _)
  | Not (Atom _) -> pe

  | And [] -> True
  | Or [] -> False
  | And [pe] -> clausify pe
  | Or [pe] -> clausify pe

  | Not (And []) -> False
  | Not (Or []) -> True
  | Not (And [pe]) -> clausify (Not pe)
  | Not (Or [pe]) -> clausify (Not pe)

  | Or pes ->
      begin
      (*
      find first conjunction
      multiply it out with the rest of pes
      then return -- we'll continue refining later after flattening again
      *)
      let (found, rest) = List.fold_right (fun pe (found, rest) ->
        match found with
        | Some _ -> (found, clausify pe :: rest)
        | None ->
            begin
              match pe with
              | And pes -> (Some pes, rest)
              | _ -> (None, clausify pe :: rest)
            end) pes (None, []) in
      match found with
      | None -> pe
      | Some pes ->
          And (List.map (fun pe' -> Or (clausify pe' :: rest)) pes)
      end

  | And pes -> And (List.map clausify pes)

  | Not True -> False
  | Not False -> True
  | Not (Or pes) -> And (List.map (fun pe -> clausify (Not pe)) pes)
  | Not (And pes) -> Or (List.map (fun pe -> clausify (Not pe)) pes)
  | Not (Not pe) -> clausify pe

(*High-powered contradiction finder*)
let rec sledgehammer (peers : pcap_expression list) (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False
  | Primitive _
  | Atom _
  | Not (Primitive _)
  | Not (Atom _) -> pe

  | Not _ -> failwith ("Expecting NNF")

  | Or pes ->
      List.map (fun pe ->
        let simped =
          And (pe :: peers)
          (*|> Expand.nnf_pcap*)
          |> trampoline_until_pred "Clausify" is_clausal clausify
          |> simplify_pcap ~with_sledgehammer:false in
        if simped = False then False else sledgehammer peers pe) pes
      |> (fun pes -> Or pes)

  | And pes ->
      Contract.fold_zip ~include_peers_as_parameters:true peers
       (fun xs x -> [sledgehammer xs x]) pes
      |> (fun pes -> match pes with
            | [] -> True
            | [pe] -> pe
            | _ -> And pes)

and sub_simp (n : int) (pe : pcap_expression) : pcap_expression =
    pe
    |> flatten
    |> Aux.diag_output_id (fun pe -> "INFO: flatten: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> simplify_pcap'
    |> Aux.diag_output_id (fun pe -> "INFO: simplify_pcap' (" ^ string_of_int (1 + n) ^ "): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> Contract.deduplicate_pe
    |> Aux.diag_output_id (fun pe -> "INFO: deduplicate_pe: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> simplify_pcap_singleton_connectives
    |> Aux.diag_output_id (fun pe -> "INFO: simplify_pcap_singleton_connectives (" ^ string_of_int (1 + n) ^ "): " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

(*Combination of simplification functions*)
and simplify_pcap ?(with_sledgehammer : bool = true) (pe : pcap_expression) : pcap_expression =
  trampoline "simplify_pcap" (fun pe ->
    simplify_pcap' pe
    |> Aux.diag_output_id (fun pe -> "INFO: simplify_pcap': " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> simplify_pcap_connectives
    |> simplify_pcap_singleton_connectives
    |> Aux.diag_output_id (fun pe -> "INFO: simplify_pcap_connectives: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> simplify_pcap_contradiction
    |> Aux.diag_output_id (fun pe -> "INFO: simplify_pcap_contradiction: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")
    |> simplify_pcap_singleton_connectives
    |> Aux.diag_output_id (fun pe -> "INFO: simplify_pcap_singleton_connectives: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

    |> Contract.subdiction_pe []
    |> Aux.diag_output_id (fun pe -> "INFO: subdiction_pe: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

    |> sub_simp 1

    |> Contract.subdisjunction_pe []
    |> Aux.diag_output_id (fun pe -> "INFO: subdisjunction_pe: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

    |> sub_simp 2

    |> Contract.polarity_subdisjunction_pe []
    |> Aux.diag_output_id (fun pe -> "INFO: polarity_subdisjunction_pe: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n")

(* FIXME distribution is disabled since it was growing formulas excessively wrt the reductions.
    |> distribute_if_instructed
*)

    |> sub_simp 3

    |> (fun pe ->
        if not with_sledgehammer || !Config.weak_inference || pe_weight pe > !Config.max_strong_inference_fmla_size then pe
        else
          pe
          |> sledgehammer []
          |> sub_simp 4)

    ) pe
