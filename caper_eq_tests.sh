#!/bin/bash -e
# Caper: a pcap expression analysis utility.
# Nim Sultana, November 2018.

function do_it() {
  LHS="$1"
  RHS="$2"
  OUTPUT_FILE=$(mktemp /tmp/caper.XXXXXXXXX)
  echo "LHS=${LHS}"
  echo "RHS=${RHS}"
  ./caper_check_eq.sh ${OUTPUT_FILE} "${LHS}" "${RHS}"

  if [ -z "${CAPER_KEEP_FILES}" ]
  then
    rm ${OUTPUT_FILE}
  else
    echo "Temporary file placed in ${OUTPUT_FILE}" >&2
  fi

  echo
}

LHS="ether proto \ip && ip proto \tcp && \
  tcp src port 443"
RHS="ether proto \ip && ip proto \tcp && \
  ip[9] = 0x6 && (ip[6:2]&0x1fff) = 0 && ip[4*(ip[0]&0xf):2] = 0x1bb"
do_it "${LHS}" "${RHS}"

LHS="(ether proto \ip && ip proto \tcp) || (ether proto \ip6 && ip6 proto \tcp)"
RHS="(ether proto \ip || ether proto \ip6) && \
  (ether proto \ip || ip6 proto \tcp) && \
  (ip proto \tcp || ether proto \ip6) && \
  (ip proto \tcp || ip6 proto \tcp)"
do_it "${LHS}" "${RHS}"

LHS="ether proto \ip && ip proto \tcp && \
  tcp src port 443 and \
  (tcp[((tcp[12] & 0xF0) >> 4) * 4] = 0x18) and \
  (tcp[((tcp[12] & 0xF0) >> 4) * 4 + 1] = 0x03) and \
  (tcp[((tcp[12] & 0xF0) >> 4) * 4 + 2] = 0x04) and \
  ((ip[2:2] - 4 * (ip[0] & 0x0F)) - 4 * ((tcp[12] & 0xF0) >> 4) > 69)"
RHS="ether proto \ip && \
  ip[9] = 0x6 && (ip[6:2]&0x1fff) = 0 && ip[4*(ip[0]&0xf):2] = 0x1bb && \
  ip[4*(ip[0]&0xf) +  ( ((ip[4*(ip[0]&0xf)+12])&0xF0) >>4)*4] = 0x18 && \
  ip[4*(ip[0]&0xf) +  ( ((ip[4*(ip[0]&0xf)+12])&0xF0) >>4)*4+1] = 0x03 && \
  ip[4*(ip[0]&0xf) +  ( ((ip[4*(ip[0]&0xf)+12])&0xF0) >>4)*4+2] = 0x04 && \
  ((ip[2:2] - 4 * (ip[0] & 0x0F)) - 4 * ((ip[4*(ip[0]&0xf) + 12] & 0xF0) >> 4) > 0x45)"
do_it "${LHS}" "${RHS}"

LHS="ether proto \ip || ether proto \vlan"
RHS="ether proto \vlan || ether proto \ip"
do_it "${LHS}" "${RHS}"
