(*
  Copyright Marelle Leon, April 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: English spec for translating pcap expressions to English
*)

open English_to_pcap_spec
open Pcap_syntax

let pcap_expr_of_header_attribute_string attr_s =
  match Parsing_support.parse_string (attr_s ^ " = 0") with
  | Atom (Eq (e, Nat_Literal 0)) -> e

let p_to_e_protos =
  [
    (Ether, "ethernet");
    (Ip, "IPv4");
    (Ip6, "IPv6");
    (Arp, "arp");
    (Rarp, "rarp");
    (Tcp, "tcp");
    (Udp, "udp");
    (Sctp, "sctp");
    (Vlan, "vlan");
    (Mpls, "mpls");
    (Icmp, "icmp");
    (Icmp6, "icmp6");
  ]

let p_to_e_dirs =
  [
    (Src, "source");
    (Dst, "destination");
    (Src_or_dst, "source or destination");
    (Src_and_dst, "source and destination");
    (Inbound, "inbound");
    (Outbound, "outbound");
    (Broadcast, "broadcast");
    (Multicast, "multicast");
  ]

let p_to_e_typs =
  [
    (Host, "host");
    (Net, "network");
    (Port_type, "port");
    (Portrange, "port range");
    (Proto, "proto");
    (Protochain, "protochain");
    (Gateway, "gateway");
    (* (Less, "less");
       (Greater, "greater") *)
  ]

let engl_string_of_proto p = List.assoc p p_to_e_protos
let engl_string_of_dir d = List.assoc d p_to_e_dirs
let engl_string_of_typ t = List.assoc t p_to_e_typs

let pcap_expr_proto_field_header_references =
  let english_csv_of_pe_s pe_s =
    match String.split_on_char ' ' pe_s with
    | [ p_s; d_s; t_s; _ ] ->
        "ref_field:"
        ^ engl_string_of_dir (List.assoc d_s e_to_p_dirs)
        ^ ","
        ^ engl_string_of_typ (List.assoc t_s e_to_p_typs)
        ^ "|"
        ^ engl_string_of_proto (Pcap_syntax_aux.proto_of_string p_s)
        ^ "|" ^ "header"
  in
  proto_field_header_references
  |> List.map (fun (pe_s, r) ->
         (pcap_expr_of_header_attribute_string r, english_csv_of_pe_s pe_s))

let pcap_expr_proto_flag_header_reference_values =
  proto_flag_header_references
  |> List.map (fun (p, (rs, attrs)) ->
         rs
         |> List.map (fun r ->
                attrs
                |> List.map (fun (a_s, attr_nums) ->
                       attr_nums
                       |> List.map (fun attr_num ->
                              ( "ref_other:" ^ a_s ^ "|"
                                ^ engl_string_of_proto p ^ "|" ^ "header",
                                pcap_expr_of_header_attribute_string attr_num )))))
  |> List.concat |> List.concat |> List.concat

let pcap_expr_proto_flag_header_references =
  let left_reference_pcap_string r attr_num = r ^ " " ^ "&" ^ " " ^ attr_num in
  let right_reference_pcap_string r attr_num = attr_num ^ " " ^ "&" ^ " " ^ r in
  let flag_references reference_pcap_string =
    proto_flag_header_references
    |> List.map (fun (p, (rs, attrs)) ->
           rs
           |> List.map (fun r ->
                  attrs
                  |> List.map (fun (a_s, attr_nums) ->
                         attr_nums
                         |> List.map (fun attr_num ->
                                ( pcap_expr_of_header_attribute_string
                                    (reference_pcap_string r attr_num),
                                  "ref_other:" ^ a_s ^ "|"
                                  ^ engl_string_of_proto p ^ "|" ^ "header" )))))
    |> List.concat |> List.concat |> List.concat
  in
  flag_references left_reference_pcap_string
  @ flag_references right_reference_pcap_string

let pcap_expr_proto_other_header_references =
  proto_other_header_references
  |> List.map (fun (p, refs) ->
         refs
         |> List.map (fun (a_s, rs) ->
                rs
                |> List.map (fun r ->
                       ( pcap_expr_of_header_attribute_string r,
                         "ref_other:" ^ a_s ^ "|" ^ engl_string_of_proto p ^ "|"
                         ^ "header" ))))
  |> List.concat |> List.concat
