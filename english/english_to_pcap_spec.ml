(*
  Copyright Marelle Leon, March 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Pcap spec for translating English expressions to pcap
*)

open Angstrom
open English_syntax
open English_parsing
open Pcap_syntax

type pcap_field_keyword =
  | TypField of typ
  | DirField of dir
  | ProtoField of proto

let e_to_p_protos =
  [
    ("ether", Ether);
    ("ethernet", Ether);
    (* ("ip", Ip); *)
    ("ip4", Ip);
    ("ipv4", Ip);
    ("IPV4", Ip);
    ("IPv4", Ip);
    ("ip6", Ip6);
    ("ipv6", Ip6);
    ("IPV6", Ip6);
    ("IPv6", Ip6);
    ("arp", Arp);
    ("rarp", Rarp);
    ("tcp", Tcp);
    ("udp", Udp);
    ("sctp", Sctp);
    ("vlan", Vlan);
    ("mpls", Mpls);
    ("icmp", Icmp);
    ("icmp6", Icmp6);
  ]

let e_to_p_dirs =
  [
    ("src", Src);
    ("source", Src);
    ("dst", Dst);
    ("destination", Dst);
    ("src or dst", Src_or_dst);
    ("source or destination", Src_or_dst);
    ("src and dst", Src_and_dst);
    ("source and destination", Src_and_dst);
    ("inbound", Inbound);
    ("outbound", Outbound);
    ("broadcast", Broadcast);
    ("multicast", Multicast);
  ]

let e_to_p_typs =
  [
    ("host", Host);
    ("address", Host);
    ("hostname", Host);
    ("net", Net);
    ("network", Net);
    ("port", Port_type);
    ("port number", Port_type);
    ("portrange", Portrange);
    ("port range", Portrange);
    ("proto", Proto);
    ("protochain", Protochain);
    ("gateway", Gateway);
    (* ("less", Less);
       ("greater", Greater) *)
  ]

let value_required_e_to_p_dirs =
  [
    ("src", Src);
    ("source", Src);
    ("dst", Dst);
    ("destination", Dst);
    ("src or dst", Src_or_dst);
    ("source or destination", Src_or_dst);
    ("src and dst", Src_and_dst);
    ("source and destination", Src_and_dst);
  ]

let valueless_e_to_p_dirs =
  [ ("broadcast", Broadcast); ("multicast", Multicast) ]

let standalone_e_to_p_dirs = [ ("inbound", Inbound); ("outbound", Outbound) ]
let standalone_proto_keyword = "protocol"
let standalone_dir_keyword = "direction"

(* TRANSLATION BACK *)

(* for attributes of headers that have pcap expression syntax for equality *)
let proto_field_header_references =
  [
    ("tcp src port _", "tcp[0 : 2]");
    ("tcp dst port _", "tcp[2 : 2]");
    ("ip src host _", "ip[12 : 4]");
    ("ip dst host _", "ip[16 : 4]");
    ("udp src port _", "udp[0 : 2]");
    ("udp dst port _", "udp[2 : 2]");
    ("sctp src port _", "sctp[0 : 2]");
    ("sctp dst port _", "sctp[2 : 2]");
    (* WARN illegal left shifting out of bounds *)
    (* ("ip6 src host _", "(ip6[16 : 4] << 32) & ip6[20 : 4]"); *)
  ]

let proto_flag_header_references =
  [
    ( Tcp,
      ( [ "tcp[tcp-flags]"; "tcp[13]" ],
        [
          ("CWR", [ "(1 << 7)" ]);
          ("ECE", [ "(1 << 6)" ]);
          ("URG", [ "(1 << 5)" ]);
          ("ACK", [ "(1 << 4)" ]);
          ("PSH", [ "(1 << 3)" ]);
          ("RST", [ "(1 << 2)" ]);
          ("SYN", [ "tcp-syn"; "(1 << 1)" ]);
          ("FIN", [ "tcp-fin"; "1" ]);
        ] ) );
  ]

let proto_other_header_references =
  [
    (Ether, []);
    ( Ip,
      [
        ("total length", [ "ip[2 : 2]" ]);
        ("protocol", [ "ip[9]" ]);
        ("header checksum", [ "ip[10 : 2]" ]);
      ] );
    (Ip6, [ ("payload length", [ "ip6[4 : 2]" ]); ("nextHeader", [ "ip6[6]" ]) ]);
    (Arp, []);
    (Rarp, []);
    ( Tcp,
      [
        ("sequence number", [ "tcp[4 : 4]" ]);
        ("acknowledgement number", [ "tcp[8 : 4]" ]);
        ("checksum", [ "tcp[16 : 2]" ]);
        ("urgent pointer", [ "tcp[18 : 2]" ]);
      ] );
    (Udp, [ ("length", [ "udp[4 : 2]" ]); ("checksum", [ "udp[6 : 2]" ]) ]);
    (Sctp, []);
    (Vlan, []);
    (Mpls, []);
    ( Icmp,
      [
        ("type", [ "icmp[0]" ]);
        ("code", [ "icmp[1]" ]);
        ("checksum", [ "icmp[2 : 2]" ]);
      ] );
    (Icmp6, [ ("nextHeader", [ "icmp6[40]" ]) ]);
  ]

let protos_grouped_by_proto =
  let group_by_protos lst =
    proto_other_header_references |> List.map fst
    |> List.map (fun p ->
           ( p,
             List.filter_map
               (fun (p', p_s) -> if p' = p then Some p_s else None)
               lst ))
  in
  e_to_p_protos |> List.map (fun (x, y) -> (y, x)) |> group_by_protos

let proto_keyword_of_engl_string x =
  Option.map (fun y -> ProtoField y) (List.assoc_opt x e_to_p_protos)

let dir_keyword_of_engl_string x =
  Option.map (fun y -> DirField y) (List.assoc_opt x e_to_p_dirs)

let typ_keyword_of_engl_string x =
  Option.map (fun y -> TypField y) (List.assoc_opt x e_to_p_typs)

let field_header_reference_string_of_field_string x =
  List.assoc_opt x proto_field_header_references

let pcap_engl_phrase_e_to_p_protos = List.map fst e_to_p_protos
let pcap_engl_phrase_e_to_p_protos = List.map fst e_to_p_protos
let pcap_engl_phrase_e_to_p_typs = List.map fst e_to_p_typs

let pcap_engl_phrase_value_required_e_to_p_dirs =
  List.map fst value_required_e_to_p_dirs

let pcap_engl_phrase_valueless_e_to_p_dirs = List.map fst valueless_e_to_p_dirs

let pcap_engl_phrase_standalone_e_to_p_dirs =
  List.map fst standalone_e_to_p_dirs

let pcap_engl_phrase_flag_header_references =
  List.map
    (fun (p, (_, rs)) -> (p, List.map fst rs))
    proto_flag_header_references

let pcap_engl_phrase_other_header_references =
  List.map (fun (p, rs) -> (p, List.map fst rs)) proto_other_header_references

let pcap_proto_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length pcap_engl_phrase_e_to_p_protos))

let pcap_proto_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length pcap_engl_phrase_e_to_p_protos))

let pcap_value_required_dir_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length
          pcap_engl_phrase_value_required_e_to_p_dirs))

let pcap_valueless_dir_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length pcap_engl_phrase_valueless_e_to_p_dirs))

let pcap_standalone_dir_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length pcap_engl_phrase_standalone_e_to_p_dirs))

let pcap_typ_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length pcap_engl_phrase_e_to_p_typs))

let pcap_proto_phrases_grouped_by_proto =
  protos_grouped_by_proto
  |> List.map (fun (p, ss) ->
         ( p,
           make_phrase
             (match_first_phrase (token_lists_of_phrases_by_length ss)) ))

let pcap_flag_header_reference_is_that_of_phrase_sequences =
  pcap_engl_phrase_flag_header_references
  |> List.map (fun (p, rs) ->
         ( p,
           make_phrase
             (match_first_phrase (token_lists_of_phrases_by_length rs))
           >>= fun s -> return [ s ] ))

let pcap_other_header_reference_is_that_of_phrase_sequences =
  pcap_engl_phrase_other_header_references
  |> List.map (fun (p, rs) ->
         ( p,
           make_phrase
             (match_first_phrase (token_lists_of_phrases_by_length rs))
           >>= fun s -> return [ s ] ))

let pcap_dir_typ_phrase_sequence =
  pcap_value_required_dir_phrase
  >>= (fun d -> pcap_typ_phrase >>= fun t -> return [ d; t ])
  <|> (pcap_value_required_dir_phrase >>= fun d -> return [ d ])
  <|> (pcap_typ_phrase >>= fun t -> return [ t ])

let pcap_standalone_proto_keyword_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length [ standalone_proto_keyword ]))

let pcap_standalone_dir_keyword_phrase =
  make_phrase
    (match_first_phrase
       (token_lists_of_phrases_by_length [ standalone_dir_keyword ]))

let pcap_with_mask_phrase = with_term "mask"
let pcap_of_length_phrase = of_size_term "length" "bits"

let pcap_numerical_packet_access_phrase =
  ordinal_collection_expression
    [ "byte"; "2 bytes"; "4 bytes"; "word" ]
    pcap_engl_phrase_e_to_p_protos "header"

let pcap_field_header_reference_is_that_of_phrase =
  attr_is_that_of_phrase pcap_dir_typ_phrase_sequence [ pcap_proto_phrase ]
    "header"

let pcap_flag_header_reference_is_that_of_phrases =
  proto_flag_header_references
  |> List.map (fun (p, _) ->
         attr_is_that_of_phrase
           (pcap_flag_header_reference_is_that_of_phrase_sequences
          |> List.assoc p)
           [ pcap_proto_phrases_grouped_by_proto |> List.assoc p ]
           "header")

let pcap_other_header_reference_is_that_of_phrases =
  proto_other_header_references
  |> List.map (fun (p, _) ->
         attr_is_that_of_phrase
           (pcap_other_header_reference_is_that_of_phrase_sequences
          |> List.assoc p)
           [ pcap_proto_phrases_grouped_by_proto |> List.assoc p ]
           "header")

let pcap_other_header_reference_with_attr_val_phrases =
  pcap_engl_phrase_other_header_references
  |> List.map (fun (p, rs) ->
         ( p,
           make_phrase
             (match_first_phrase (token_lists_of_phrases_by_length rs)) ))

let pcap_flag_header_reference_with_existing_flags_phrase_lists =
  pcap_engl_phrase_flag_header_references
  |> List.map (fun (p, rs) ->
         ( p,
           rs
           |> List.map (Fun.flip List.cons [])
           |> List.map (fun x ->
                  make_phrase
                    (match_first_phrase (token_lists_of_phrases_by_length x)))
         ))

let pcap_relation_expression =
  relation_expression
    ([
       pcap_numerical_packet_access_phrase;
       pcap_field_header_reference_is_that_of_phrase;
     ]
    @ pcap_flag_header_reference_is_that_of_phrases
    @ pcap_other_header_reference_is_that_of_phrases)

let pcap_examining_relation_expression =
  examining_relation_expression
    ([
       pcap_numerical_packet_access_phrase;
       pcap_field_header_reference_is_that_of_phrase;
     ]
    @ pcap_flag_header_reference_is_that_of_phrases
    @ pcap_other_header_reference_is_that_of_phrases)

let pcap_is_that_of_expression =
  is_that_of_expression pcap_dir_typ_phrase_sequence
    [
      pcap_with_mask_phrase;
      pcap_of_length_phrase;
      pcap_typ_phrase;
      pcap_proto_phrase;
      unidentified_term;
    ]

let pcap_attr_is_that_of_expression =
  attr_is_that_of_expression pcap_dir_typ_phrase_sequence [ pcap_proto_phrase ]
    "some" "header"
    [
      pcap_with_mask_phrase;
      pcap_of_length_phrase;
      pcap_typ_phrase;
      pcap_proto_phrase;
      unidentified_term;
    ]

let pcap_keyword_is_that_of_expression =
  keyword_is_that_of_expression
    [
      (pcap_standalone_proto_keyword_phrase, pcap_proto_phrase);
      (pcap_standalone_dir_keyword_phrase, pcap_standalone_dir_phrase);
    ]

let pcap_that_has_expression =
  that_has_expression pcap_proto_phrase pcap_dir_typ_phrase_sequence
    [
      pcap_with_mask_phrase;
      pcap_of_length_phrase;
      pcap_typ_phrase;
      pcap_proto_phrase;
      unidentified_term;
    ]

let pcap_with_attr_val_expressions =
  proto_other_header_references
  |> List.map (fun (p, _) ->
         with_attr_val_expression
           (pcap_proto_phrases_grouped_by_proto |> List.assoc p)
           (pcap_other_header_reference_with_attr_val_phrases |> List.assoc p)
           [ unidentified_term ])

let pcap_with_existing_flags_expressions =
  proto_flag_header_references
  |> List.map (fun (p, _) ->
         with_existing_attrs_expression
           (pcap_proto_phrases_grouped_by_proto |> List.assoc p)
           "flag" "flags"
           (pcap_flag_header_reference_with_existing_flags_phrase_lists
          |> List.assoc p))

let pcap_that_is_expression =
  that_is_expression pcap_proto_phrase
    [ pcap_valueless_dir_phrase; unidentified_term ]

let pcap_of_type_expression =
  of_type_expression [ pcap_proto_phrase; pcap_standalone_dir_phrase ]

(* UH OH: must attempt 'relation_expression's first
          because 'is' is a prefix of 'is greater than' *)
let pcap_english_expression =
  english_expression
    ([
       pcap_relation_expression;
       pcap_examining_relation_expression;
       pcap_attr_is_that_of_expression;
       pcap_is_that_of_expression;
       pcap_keyword_is_that_of_expression;
     ]
    @ pcap_with_attr_val_expressions @ pcap_with_existing_flags_expressions
    @ [
        pcap_that_has_expression;
        pcap_that_is_expression;
        pcap_of_type_expression;
        boolean_atom_expression;
      ])

(** parse a pcap english expression into an English AST *)
let parse_engl_pcap_expression =
  parse_string ~consume:Prefix pcap_english_expression
