(*
  Copyright Marelle Leon, March 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module:  Translating English expressions into Pcap expressions
*)

open English_syntax
open English_to_pcap_spec
open Pcap_syntax

let field_of_phrase_term (ph : term) : (field, string) result =
  match ph with
  | Phrase ph_s -> (
      match
        ( proto_keyword_of_engl_string ph_s,
          dir_keyword_of_engl_string ph_s,
          typ_keyword_of_engl_string ph_s )
      with
      | Some (ProtoField p), _, _ -> Ok (Some p, None, None)
      | _, Some (DirField d), _ -> Ok (None, Some d, None)
      | _, _, Some (TypField t) -> Ok (None, None, Some t)
      | _ -> Error ("invalid field (proto/dir/typ) value: '" ^ ph_s ^ "'"))

(* fields can be one of [proto; dir; typ], [proto; dir;], [proto; typ], [dir; typ], [typ] *)
let field_of_phrase_terms (flds : term list) =
  List.fold_left
    (fun (fld : (field, string) result) (t : term) ->
      match fld with
      | Ok (None, None, None) -> field_of_phrase_term t
      | Ok (Some p, None, None) -> (
          match t with
          | Phrase ph_s -> (
              match
                ( dir_keyword_of_engl_string ph_s,
                  typ_keyword_of_engl_string ph_s )
              with
              | Some (DirField d), _ -> Ok (Some p, Some d, None)
              | _, Some (TypField t) -> Ok (Some p, None, Some t)
              | _ -> Error ("invalid field (dir/typ) value: '" ^ ph_s ^ "'")))
      | Ok (p_opt, Some d, None) -> (
          match t with
          | Phrase ph_s -> (
              match typ_keyword_of_engl_string ph_s with
              | Some (TypField t) -> Ok (p_opt, Some d, Some t)
              | _ -> Error ("invalid field (typ) value: '" ^ ph_s ^ "'")))
      | Ok (_, _, Some t) ->
          Error
            ("too many field values or field values in wrong order (last value \
              is: '"
            ^ Pcap_syntax_aux.string_of_typ t
            ^ "')")
      | Error err -> Error err)
    (Ok (None, None, None))
    flds

let value_atom_of_term = function
  | Phrase ph_s -> (
      let proto_pcap_string_of_engl_string_opt s =
        let f = proto_keyword_of_engl_string s in
        match f with
        | Some (ProtoField p) -> Some (Pcap_syntax_aux.string_of_proto p)
        | _ -> None
      in
      match proto_pcap_string_of_engl_string_opt ph_s with
      | Some s -> Ok (Escaped_String s)
      | None -> Ok (Escaped_String ph_s))
  | With_term ("mask", l, r) -> Ok (String (l ^ " mask " ^ r))
  | OfSize_term ("length", "bits", l, r) -> Ok (String (l ^ "/" ^ r))
  | UnidentifiedString x -> Ok (String x)
  | t ->
      Error
        ("not simple value atom: '" ^ English_to_string.string_of_term t ^ "'")

let value_atom_list_of_term = function
  | TermList vals -> List.map value_atom_of_term vals
  | v -> [ value_atom_of_term v ]

let combine_field_with_values (fld : (field, string) result)
    (val_results : (value_atom, string) result list) :
    (pcap_expression, string) result =
  match fld with
  | Ok f -> (
      let vals =
        List.fold_right
          (fun value rest ->
            match value with
            | Ok v -> (
                match rest with Ok lst -> Ok (v :: lst) | Error e -> Error e)
            | Error e -> Error e)
          val_results (Ok [])
      in
      match vals with
      | Ok (v :: []) -> Ok (Primitive (f, v))
      | Ok (v1 :: vs) ->
          (* a disjunction of the form "<proto> <value1> || <value2> || <value3> || ..." *)
          let f0 = (None, None, None) in
          Ok
            (Or (Primitive (f, v1) :: List.map (fun v -> Primitive (f0, v)) vs))
      | Error e -> Error e)
  | Error e -> Error e

let string_of_ordinal_reference_term
    (number, ordinal_name_s, collection_name, obj_name) =
  match obj_name with
  | "header" -> (
      match field_of_phrase_term (Phrase collection_name) with
      | Ok (Some p, _, _) ->
          let header_name_s = Pcap_syntax_aux.string_of_proto p in
          let no_bytes =
            match ordinal_name_s with
            | "byte" -> ""
            | "2 bytes" -> " : 2"
            | "4 bytes" | "word" -> " : 4"
          in
          " " ^ header_name_s ^ "[" ^ number ^ no_bytes ^ "] ")

let string_of_attr_term (ls, m) =
  let p =
    match field_of_phrase_term m with Ok (Some p, _, _) -> p
    (* | _ -> Error "" *)
  in
  match ls with
  | [ Phrase ph_s ]
    when List.exists (fun (p', _) -> p' = p) proto_flag_header_references
         && List.exists (( = ) ph_s)
              (proto_flag_header_references |> List.assoc p |> snd
             |> List.map fst) ->
      (* make corresponding pcap expr string from list (first one of each) *)
      proto_flag_header_references |> List.assoc p |> fun (refs, attrs) ->
      let r = List.hd refs in
      let attr_num = attrs |> List.assoc ph_s |> fun nums -> List.hd nums in
      r ^ " " ^ "&" ^ " " ^ attr_num
  | [ Phrase ph_s ]
    when List.exists (fun (p', _) -> p' = p) proto_other_header_references
         && List.exists (( = ) ph_s)
              (proto_other_header_references |> List.assoc p |> List.map fst) ->
      (* get corresponding pcap expr string from list (first one) *)
      proto_other_header_references |> List.assoc p |> fun attrs ->
      let attr_pcap_s = attrs |> List.assoc ph_s |> fun ss -> List.hd ss in
      attr_pcap_s
  | _ -> (
      let pe_res =
        let fld = field_of_phrase_terms (m :: ls) in
        let vals = [ Ok (String "_") ] in
        combine_field_with_values fld vals
      in
      let pcap_s =
        match pe_res with
        | Ok pe -> Pcap_syntax_aux.string_of_pcap_expression pe
        (* | Error e -> Error "" *)
      in
      match field_header_reference_string_of_field_string pcap_s with
      | Some h_s -> h_s)

let string_of_arithmetic (start_s, expr_pairs) =
  let expr_strs =
    List.map
      (fun (l, r_s, _) ->
        let l_s =
          match l with
          | OrdinalReference (number, ordinal_name_s, collection_name, obj_name)
            ->
              string_of_ordinal_reference_term
                (number, ordinal_name_s, collection_name, obj_name)
          | AttributeIsThatOf_term (ls, middle, _) ->
              string_of_attr_term (ls, middle)
        in
        " " ^ l_s ^ " " ^ r_s)
      expr_pairs
  in
  let relation_s =
    let rest_s = String.concat "" expr_strs in
    (* makes sure no leading/trailing whitespace *)
    match (start_s, rest_s) with
    | "", _ -> rest_s
    | _, "" -> start_s
    | _, _ -> start_s ^ " " ^ rest_s
  in
  relation_s

let pcap_expression_of_clause (c : clause) : (pcap_expression, string) result =
  match c with
  | OfType r -> (
      match r with
      | Phrase _ -> (
          let fld = field_of_phrase_terms [ r ] in
          match fld with Ok f -> Ok (Primitive (f, Nothing)))
      | t ->
          Error
            ("incorrect proto/dir in 'of type' expression: '"
            ^ English_to_string.string_of_term t
            ^ "'"))
  | ThatIs (l, r) -> (
      match r with
      | Phrase _ -> (
          let fld = field_of_phrase_terms [ l; r ] in
          match fld with Ok f -> Ok (Primitive (f, Nothing)))
      | t ->
          Error
            ("incorrect dir/type in 'that is' expression: '"
            ^ English_to_string.string_of_term t
            ^ "'"))
  | AttributeIsThatOf (ls, None, r, _) ->
      let fld = field_of_phrase_terms ls in
      let vals = value_atom_list_of_term r in
      combine_field_with_values fld vals
  | AttributeIsThatOf (ls, Some m, r, _) ->
      let fld = field_of_phrase_terms (m :: ls) in
      let vals = value_atom_list_of_term r in
      combine_field_with_values fld vals
  | IsThatOf (ls, r) ->
      let fld = field_of_phrase_terms ls in
      let vals = value_atom_list_of_term r in
      combine_field_with_values fld vals
  | KeywordIsThatOf (_, r) -> (
      match r with
      | Phrase _ -> (
          let fld = field_of_phrase_terms [ r ] in
          match fld with Ok f -> Ok (Primitive (f, Nothing)))
      | t ->
          Error
            ("incorrect proto/dir in '<keyword> is that of' expression: '"
            ^ English_to_string.string_of_term t
            ^ "'"))
  | ThatHas (l, ms, r) ->
      let fld = field_of_phrase_terms (l :: ms) in
      let vals = value_atom_list_of_term r in
      combine_field_with_values fld vals
  | WithAttributeValue (l, Phrase ph_s, UnidentifiedString v) ->
      let p =
        match field_of_phrase_term l with Ok (Some p, _, _) -> p
        (* | _ -> Error "" *)
      in
      let left_arith_s =
        (* get corresponding pcap expr string from list (first one) *)
        proto_other_header_references |> List.assoc p |> fun attrs ->
        let attr_pcap_s = attrs |> List.assoc ph_s |> fun ss -> List.hd ss in
        attr_pcap_s
      in
      let right_arith_s = v in
      let relation_s = left_arith_s ^ " = " ^ right_arith_s in
      (* FIXME - can't check for syntax errors here *)
      Ok (Parsing_support.parse_string relation_s)
  | WithExistingAttributes (l, Phrase ph_s, Phrase "flag") ->
      let p =
        match field_of_phrase_term l with Ok (Some p, _, _) -> p
        (* | _ -> Error "" *)
      in
      let left_arith_s, right_arith_s =
        (* make corresponding pcap expr string from list (first one of each) *)
        proto_flag_header_references |> List.assoc p |> fun (refs, attrs) ->
        let r = List.hd refs in
        let attr_num = attrs |> List.assoc ph_s |> fun nums -> List.hd nums in
        (r ^ " " ^ "&" ^ " " ^ attr_num, attr_num)
      in
      let relation_s = left_arith_s ^ " = " ^ right_arith_s in
      (* FIXME - can't check for syntax errors here *)
      Ok (Parsing_support.parse_string relation_s)
  | WithExistingAttributes (l, TermList rs, Phrase "flags") ->
      let p =
        match field_of_phrase_term l with Ok (Some p, _, _) -> p
        (* | _ -> Error "" *)
      in
      let left_arith_s, right_arith_s =
        (* make corresponding pcap expr string from list (first one of each) *)
        proto_flag_header_references |> List.assoc p |> fun (refs, attrs) ->
        let r = List.hd refs in
        let attr_num ph_s =
          attrs |> List.assoc ph_s |> fun nums -> List.hd nums
        in
        let attr_nums =
          rs
          |> List.map (fun (Phrase ph_s) -> attr_num ph_s)
          |> String.concat " | "
        in
        (r ^ " " ^ "&" ^ " " ^ "(" ^ attr_nums ^ ")", attr_nums)
      in
      let relation_s = left_arith_s ^ " = " ^ right_arith_s in
      (* FIXME - can't check for syntax errors here *)
      Ok (Parsing_support.parse_string relation_s)
  | Relation_clause
      ( Phrase comp_s,
        Arithmetic (left_start_s, left_expr_pairs),
        Arithmetic (right_start_s, right_expr_pairs) )
  | ExaminingRelation_clause
      ( Phrase comp_s,
        _,
        Arithmetic (left_start_s, left_expr_pairs),
        Arithmetic (right_start_s, right_expr_pairs) ) ->
      let left_arith_s = string_of_arithmetic (left_start_s, left_expr_pairs) in
      let right_arith_s =
        string_of_arithmetic (right_start_s, right_expr_pairs)
      in
      let relation_s = left_arith_s ^ " " ^ comp_s ^ " " ^ right_arith_s in
      (* FIXME - can't check for syntax errors here *)
      Ok (Parsing_support.parse_string relation_s)
  | Unit (UnidentifiedString x) -> Ok (Primitive ((None, None, None), String x))

let rec pcap_expression_result_of_engl_expression (e : engl_expression) :
    (pcap_expression, string) result =
  match e with
  | Clause c -> pcap_expression_of_clause c
  | And_expr xs -> (
      List.fold_right
        (fun curr rest ->
          match (curr, rest) with
          | Ok y, Ok ys -> Ok (y :: ys)
          | Ok _, Error err -> Error err
          | Error err, _ -> Error err)
        (List.map pcap_expression_result_of_engl_expression xs)
        (Ok [])
      |> function
      | Ok pes -> Ok (And pes)
      | Error err -> Error err)
  | Or_expr xs -> (
      List.fold_right
        (fun curr rest ->
          match (curr, rest) with
          | Ok y, Ok ys -> Ok (y :: ys)
          | Ok _, Error err -> Error err
          | Error err, _ -> Error err)
        (List.map pcap_expression_result_of_engl_expression xs)
        (Ok [])
      |> function
      | Ok pes -> Ok (Or pes)
      | Error err -> Error err)
  | Not_expr x -> (
      match pcap_expression_result_of_engl_expression x with
      | Ok pe -> Ok (Not pe)
      | err -> err)
  | True_expr -> Ok True
  | False_expr -> Ok False

let pcap_expression_result_of_english_string (engl_s : string) =
  match parse_engl_pcap_expression engl_s with
  | Error err -> Error "Parsing error: invalid English"
  | Ok e -> (
      match pcap_expression_result_of_engl_expression e with
      | Ok e -> Ok e
      | Error err -> Error ("Semantic error: " ^ err))
