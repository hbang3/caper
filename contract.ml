(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Simplification for pcap expressions
*)

open Pcap_syntax

(*FIXME move somewhere more general*)
let rec fold_zip' (include_peers_as_parameters : bool) (acc : 'b list) (pre : 'a list) (f : 'a list -> 'a -> 'b list) (l : 'a list) : 'b list =
  match l with
  | [] -> acc
  | (x :: xs) ->
      let immediate_pre' = pre @ (if include_peers_as_parameters then xs else []) in
      let pre' = pre @ (if include_peers_as_parameters then [x] else []) in
      fold_zip' include_peers_as_parameters (acc @ f immediate_pre' x) pre' f xs
let fold_zip ?(include_peers_as_parameters : bool = false) premises f l =
  fold_zip' include_peers_as_parameters [] premises f l

(*FIXME move to Pcap_syntax or Pcap_syntax_aux?*)
let side_effecting_proto (proto : proto) : bool =
  match proto with
  | Vlan
  | Mpls -> true
  | _ -> false
let contains_SE_proto_pe (pe : pcap_expression) : bool =
  let protos =
    Names.protos_in_pe pe Names.StringSet.empty
    |> Names.protos_of_strings in
  Names.ProtoSet.is_empty (Names.ProtoSet.filter side_effecting_proto protos)
  |> not

(*Gather all conjoined propositions in a formula even if they're nested in other conjunctions.*)
let rec all_conjuncts_pe (pe : pcap_expression) : pcap_expression list = (*FIXME this function isn't used*)
  match pe with
  | True
  | False
  | Primitive _
  | Atom _
  | Not (Primitive _)
  | Not (Atom _)
  | Or _ -> [pe]
  | Not (Not pe') -> all_conjuncts_pe pe'
  | Not (And pes) -> [Or (List.map (fun pe -> Not pe) pes)]

  | And pes ->
      List.concat (List.map all_conjuncts_pe pes)
  | Not (Or pes) ->
      let pes' = List.map (fun pe -> Not pe) pes in
      List.concat (List.map all_conjuncts_pe pes')

  | Not _ -> failwith "TODO"(*FIXME ensure that only NNF-normalised formulas as given as parameters?*)

(*Check for subsumption, for example "ip" and "ip6" don't subsume each other,
  but "ip proto \tcp" subsumes "tcp" -- I implicitly assume that this function
    is called after expansion has been carried out -- i.e., "tcp" has been
    expanded into "ip proto \tcp" for example, which permits us to drop the
    former.  *)
let subsumes (pe1 : pcap_expression) (pe2 : pcap_expression) : bool =
  match pe1, pe2 with
  | Primitive ((net_proto, None, Some Proto), Escaped_String proto),
    Primitive ((Some other_proto, None, None), Nothing)
    when proto = Pcap_syntax_aux.string_of_proto other_proto -> true
    (*FIXME consider case where pe1 = pe2 -- but ensure we're not comparing with self*)
    (*FIXME consider Atom values*)
  | _ -> false
let rec subsumption_pe (pe : pcap_expression) : pcap_expression =
  let filter_subsumed (pes : pcap_expression list) : pcap_expression list =
    List.fold_right (fun (pe : pcap_expression) (acc : pcap_expression list) ->
      let subsumed =
        List.fold_right (fun (pe' : pcap_expression) (acc : pcap_expression option) ->
          if acc <> None then acc
          else if subsumes pe' pe then
            begin
              Aux.diag_output ("INFO: subsumption_pe: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ " by " ^ Pcap_syntax_aux.string_of_pcap_expression pe' ^ "\n");
              Some pe'
            end
          else None) pes None in
      if subsumed = None then
        begin
(*
          Aux.diag_output ("INFO: Not subsumption_pe: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n");
*)
          (subsumption_pe pe) :: acc
        end
      else acc) pes [] in
  match pe with
  | Primitive _
  | Atom _
  | True
  | False -> pe
  | Not pe' -> Not (subsumption_pe pe')
  | And pes -> And (filter_subsumed pes)
  | Or pes -> Or (filter_subsumed pes)

(*Check if a formula is subsumed by an assumption. Returns the subsuming expression if one is found.*)
let entailed (premises : pcap_expression list) (pe : pcap_expression) : pcap_expression option =
  List.fold_right (fun pe' acc ->
    if acc <> None then acc
    else if pe = pe' || subsumes pe' pe then Some pe'
    else None) premises None

(*FIXME DRY from simplify_pcap_contradiction*)
let conflict (pe1 : pcap_expression) (pe2 : pcap_expression) : bool =
match pe1, pe2 with
(*| Primitive _, Atom _ -> conflict pe2 pe1*)
| Atom re, Primitive ((Some proto, None, Some Proto), Escaped_String sub_proto) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          (proto = Tcp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Sctp protos)) ||
                          (proto = Udp && (Names.ProtoSet.mem Tcp protos || Names.ProtoSet.mem Sctp protos)) ||
                          (proto = Sctp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Tcp protos)) ||
                          (proto = Ip && Names.ProtoSet.mem Ip6 protos) ||
                          (proto = Ip6 && Names.ProtoSet.mem Ip protos) ||
                          (Pcap_syntax_aux.string_of_proto proto = sub_proto) ||
                          (sub_proto = Pcap_syntax_aux.string_of_proto Tcp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Sctp protos)) ||
                          (sub_proto = Pcap_syntax_aux.string_of_proto Udp && (Names.ProtoSet.mem Tcp protos || Names.ProtoSet.mem Sctp protos)) ||
                          (sub_proto = Pcap_syntax_aux.string_of_proto Sctp && (Names.ProtoSet.mem Udp protos || Names.ProtoSet.mem Tcp protos)) ||
                          (sub_proto = Pcap_syntax_aux.string_of_proto Ip && Names.ProtoSet.mem Ip6 protos) ||
                          (sub_proto = Pcap_syntax_aux.string_of_proto Ip6 && Names.ProtoSet.mem Ip protos)
                        end
| Atom re, Primitive ((Some proto, _, Some Host), IPv4_Address _)
| Atom re, Primitive ((Some proto, _, Some Net), IPv4_Network _)
| Atom re, Primitive ((Some proto, _, Some Net), IPv4_Network_Masked _) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          proto = Ip6 || Names.ProtoSet.mem Ip6 protos
                        end
| Atom re, Primitive ((_, _, Some Net), IPv4_Network _)
| Atom re, Primitive ((_, _, Some Net), IPv4_Network_Masked _)
| Atom re, Primitive ((_, _, Some Host), IPv4_Address _) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          Names.ProtoSet.mem Ip6 protos
                        end
| Atom re, Primitive ((Some proto, _, Some Host), IPv6_Address _)
| Atom re, Primitive ((Some proto, _, Some Net), IPv6_Network _) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          proto = Ip || Names.ProtoSet.mem Ip protos ||
                          proto = Arp || Names.ProtoSet.mem Arp protos ||
                          proto = Rarp || Names.ProtoSet.mem Rarp protos
                        end
| Atom re, Primitive ((_, _, Some Net), IPv6_Network _)
| Atom re, Primitive ((_, _, Some Host), IPv6_Address _) ->
                        begin
                          let protos =
                            Names.protos_in_re re Names.StringSet.empty
                            |> Names.protos_of_strings in
                          Names.ProtoSet.mem Ip protos ||
                          Names.ProtoSet.mem Arp protos ||
                          Names.ProtoSet.mem Rarp protos
                        end



| Primitive ((Some proto, None, Some Proto), Escaped_String sub_proto), Primitive ((Some proto', None, Some Proto), Escaped_String sub_proto') ->
    (proto = proto' && sub_proto <> sub_proto') ||
    (proto = Ip && proto' = Ip6) || (proto = Ip6 && proto' = Ip)


| Primitive ((Some proto, _, _), _), Primitive ((Some proto', None, Some Proto), Escaped_String sub_proto') -> (*FIXME check the opposite direction*)
    (proto = Ip && proto' = Ip6) || (proto = Ip6 && proto' = Ip)

| Primitive ((Some proto, _, _), _), Primitive ((Some proto', _, _), _) ->
    (proto = Ip && proto' = Ip6) || (proto = Ip6 && proto' = Ip)

| x, Not y
| Not x, y -> x = y

| _, _ -> false (*FIXME might be too weak*)

(*Check if a formula doesn't disagree with an assumption. Returns the conflicting expression if one is found.*)
let consistent (premises : pcap_expression list) (pe : pcap_expression) : pcap_expression option =
  List.fold_right (fun pe' acc ->
    if acc <> None then acc
    else if pe = Not pe' || pe' = Not pe || conflict pe' pe || conflict pe pe' then Some pe'
    else None) premises None

let split_consistent_inconsistent premises pes =
  List.fold_right (fun pe (cons_acc, incons_acc) ->
    if consistent premises pe <> None then (cons_acc, pe :: incons_acc)
    else (pe :: cons_acc, incons_acc)) pes ([], [])
let split_subsumed_unsubsumed premises pes =
  List.fold_right (fun pe (sub_acc, unsub_acc) ->
    if entailed premises pe <> None then (pe :: sub_acc, unsub_acc)
    else (sub_acc, pe :: unsub_acc)) pes ([], [])

(*Rewrite a formula to simplify it wrt entailed or inconsistent propositions*)
(*If entailed (then must be consistent) then rewrite with True;
  If not entailed then check if consistent:
    - if inconsistent then rewrite with False;
    - otherwise keep formula as it is.*)
let rec subdiction_pe (premises : pcap_expression list) (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False -> pe

  | Primitive _
  | Atom _
  | Not (Primitive _)
  | Not (Atom _) ->
      if entailed premises pe <> None then True
      else if consistent premises pe <> None then False
      else pe

  | And pes ->
      (*Check if contradicted -- one subformula contradicted*)
      (*Check if fully subsumed -- all subformulas subsumed*)
      (*Check if partly subsumed -- and work on its subformulas*)
      let (cons, incons) = split_consistent_inconsistent premises pes in
      assert (List.length pes = List.length cons + List.length incons);
      let (subs, unsubs) = split_subsumed_unsubsumed premises pes in
      assert (List.length pes = List.length subs + List.length unsubs);
      if incons <> [] then False
      else if unsubs = [] then True
      else
        fold_zip ~include_peers_as_parameters:true premises
         (fun xs x -> [subdiction_pe xs x]) unsubs
        |> (fun pes -> match pes with
              | [] -> True
              | [pe] -> pe
              | _ -> And pes)

  | Or pes ->
      (*Check if fully subsumed -- one subformula subsumed*)
      (*Check if contradicted -- all subformulas contradicted*)
      (*Check if partly contradicted -- and work on its subformulas*)
      let (cons, incons) = split_consistent_inconsistent premises pes in
      assert (List.length pes = List.length cons + List.length incons);
      let (subs, unsubs) = split_subsumed_unsubsumed premises pes in
      assert (List.length pes = List.length subs + List.length unsubs);
      if subs <> [] then True
      else if List.length incons = List.length pes then False
      else
        begin
        assert (List.length unsubs = List.length pes);
        fold_zip ~include_peers_as_parameters:false premises
         (fun xs x -> [subdiction_pe xs x]) pes
        |> (fun pes -> match pes with
              | [] -> False
              | [pe] -> pe
              | _ -> Or pes)
        end

  | Not _ -> pe (*FIXME could be more conservative: failwith (Aux.error_output_prefix ^ ": subdiction_pe:" ^ " " ^ "Expecting NNF")*)


(*Rewrite a formula to simplify it wrt entailed propositions.
Similar to subdiction_pe but focuses on disjunctions:
If a proposition is entailed from the "premise" set then rewrite to False.
*)
let rec subdisjunction_pe (premises : pcap_expression list) (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False -> pe

  | Primitive _
  | Atom _
  | Not (Primitive _)
  | Not (Atom _) ->
      if entailed premises pe <> None then False
      else pe

  | And pes ->
      let (subs, unsubs) = split_subsumed_unsubsumed premises pes in
      assert (List.length pes = List.length subs + List.length unsubs);
      if subs <> [] then False
      else
        begin
        assert (List.length unsubs = List.length pes);
        fold_zip ~include_peers_as_parameters:false premises
         (fun xs x -> [subdisjunction_pe xs x]) unsubs
        |> (fun pes -> match pes with
              | [] -> failwith "Impossible"
              | [pe] -> pe
              | _ -> And pes)
        end

  | Or pes ->
      let (subs, unsubs) = split_subsumed_unsubsumed premises pes in
      assert (List.length pes = List.length subs + List.length unsubs);
      if unsubs = [] then False
      else
        fold_zip ~include_peers_as_parameters:true premises
         (fun xs x -> [subdisjunction_pe xs x]) pes
        |> (fun pes -> match pes with
              | [] -> failwith "Impossible"
              | [pe] -> pe
              | _ -> Or pes)

  | Not _ -> pe (*FIXME could be more conservative: failwith (Aux.error_output_prefix ^ ": subdisjunction_pe:" ^ " " ^ "Expecting NNF")*)

(*
Another form of subsumption:
  ( Literal | (~Literal & ..1..) | ..2..) -> ( Literal | (True & ..1..) | ..2..)
  ( Literal | (Literal & ..1..) | ..2..) -> ( Literal | (False & ..1..) | ..2..)

Instead of using entailment or consistency as defined above, this only looks at strictly conflicting literals -- i.e., true and false polarity of the same atom/primitive.
*)
let rec polarity_subdisjunction_pe (premises : pcap_expression list) (pe : pcap_expression) : pcap_expression =
  match pe with
  | True
  | False -> pe

  | Primitive _
  | Atom _
  | Not (Primitive _)
  | Not (Atom _) ->
      List.fold_right (fun pe' acc ->
        if False = acc || True = acc then acc
        else if pe' = Not pe || pe = Not pe' then True
        else if pe' = pe then False
        else acc) premises pe

  | And pes ->
      And (List.map (polarity_subdisjunction_pe premises) pes)

  | Or pes ->
      (*FIXME use "all_conjuncts_pe pe" to work through conjuncts?*)
      fold_zip ~include_peers_as_parameters:true premises
       (fun xs x -> [polarity_subdisjunction_pe xs x]) pes
      |> (fun pes -> match pes with
            | [] -> failwith "Impossible"
            | [pe] -> pe
            | _ -> Or pes)

  | Not _ -> pe (*FIXME could be more conservative: failwith (Aux.error_output_prefix ^ ": polarity_subdisjunction_pe:" ^ " " ^ "Expecting NNF")*)


(*Remove redundant duplicate subformulas -- e.g., turning "A & A" into "A"*)
let rec deduplicate_pe (pe : pcap_expression) : pcap_expression =
  let deduplicate_pes pes =
    List.fold_right (fun pe acc ->
      let pe' = deduplicate_pe pe in
      if List.exists (fun pe ->
        if pe = pe' then
          if not (contains_SE_proto_pe pe) then
            begin
              Aux.diag_output ("INFO: Deduplicating: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n");
              true
            end
          else
            begin
              Aux.diag_output ("INFO: Not deduplicating: " ^ Pcap_syntax_aux.string_of_pcap_expression pe ^ "\n");
              false
            end
        else false) acc then
          acc
      else pe' :: acc) pes [] in
  match pe with
  | Primitive _
  | Atom _
  | True
  | False -> pe
  | Not pe' -> Not (deduplicate_pe pe')
  | And pes
  | Or pes ->
      begin
      let pes' = deduplicate_pes pes in
        match pes' with
        | [] ->
          begin
            match pe with
            | And _ -> True
            | Or _ -> False
            | _ -> failwith "Impossible"
          end
        | [pe] -> pe
        | _ ->
          begin
            match pe with
            | And _ -> And pes'
            | Or _ -> Or pes'
            | _ -> failwith "Impossible"
          end
      end

(*
This only works for conjunctions of primitives/atoms:
pull out all the protocols that are mentioned in that conjunction, and if there are any incompatible protocols (e.g., ip and ip6, tcp and udp, etc) then rewrite the whole thing to False.

NOTE: Assumes that formulas have been flattened.
*)
let rec detect_inconsistency_pe (pe : pcap_expression) : pcap_expression =
  match pe with
  | Primitive _
  | Atom _
  | True
  | False -> pe
  | Not pe' -> Not (detect_inconsistency_pe pe')
  | Or pes -> Or (List.map detect_inconsistency_pe pes)
  | And pes ->
      let pes' = List.map detect_inconsistency_pe pes in
      let only_prim_or_atoms pes = List.fold_right (fun pe acc ->
        match pe with
        | Primitive _
        | Atom _ -> true && acc
        | _ -> false) pes true in
      if not (only_prim_or_atoms pes') then And pes'
      else
        let protos_and_layers =
          Pcap_syntax_aux.list_protocols [] pe
          |> List.map (fun proto -> (proto, Layering.layer_of_proto proto)) in
        let conflict_found = (*FIXME inefficient*) List.fold_right (fun (proto1, l1) acc ->
          List.fold_right (fun (proto2, l2) acc ->
            if acc then acc else
              if proto1 <> proto2 && l1 = l2 then true else false) protos_and_layers acc) protos_and_layers false
      in if conflict_found then False else And pes'
