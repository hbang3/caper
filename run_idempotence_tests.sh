#!/bin/bash
# Caper: a pcap expression analysis utility.
# Run regression tests wrt idempotence.
# Nik Sultana, March 2023

# FIXME sensitive to the directory in which this is run
tests/disambiguate.sh "./caper.byte -1stdin -q" tests/idempotence_test.sh
