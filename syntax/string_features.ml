(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Matchers for string features in pcap expressions
*)

let is_packet_name (id : string) : bool =
  let regex = Str.regexp "[a-z][a-zA-Z0-9_]*" in
  Str.string_match regex id 0

let is_unsignedint (id : string) : bool =
  let regex = Str.regexp "[0-9]+" in
  Str.string_match regex id 0

let is_var_name (id : string) : bool =
  let regex = Str.regexp "[A-Z][a-zA-Z0-9_]*" in
  Str.string_match regex id 0

let is_bracketed (id : string) : bool =
  let regex = Str.regexp "^(.*)$" in
  Str.string_match regex id 0

let assert_packet_name (id : string) : unit =
  if is_packet_name id then ()
  else failwith ("'" ^ id ^ "' is not a packet name")

let assert_unsignedint (id : string) : unit =
  if is_unsignedint id then ()
  else failwith ("'" ^ id ^ "' is not an unsigned integer")

let assert_var_name (id : string) : unit =
  if is_var_name id then ()
  else failwith ("'" ^ id ^ "' is not a variable name")
