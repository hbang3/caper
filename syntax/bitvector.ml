(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Syntax for bit-vector logic
*)

type width = int (*in bits*)
type nat = int
type offset = int (*in bits*)

let bpf_size_word = 32
let bpf_size_halfword = 16
let bpf_size_byte = 8

(*NOTE in this implementation, arithmetic on bitvectors is unsigned*)
type bv_exp =
  (*Hole is used for embedding SMTLib syntax directly. This is also used
    for SMTLib-level names, "packet".*)
  | Hole of width option * string

  | Const of width option * nat
  | Concat of width option * bv_exp * bv_exp
  | Extract of offset * width * bv_exp
  | BNot of width option * bv_exp
  | BAnd of width option * bv_exp * bv_exp
  | BOr of width option * bv_exp * bv_exp
  | BXor of width option * bv_exp * bv_exp
  | BNegate of width option * bv_exp
  | BPlus of width option * bv_exp * bv_exp
  | BMul of width option * bv_exp * bv_exp
  | BDiv of width option * bv_exp * bv_exp
  | BRem of width option * bv_exp * bv_exp
  | BMod of width option * bv_exp * bv_exp
  | BMinus of width option * bv_exp * bv_exp
  | ShLeft of width option * bv_exp * bv_exp
  | LShRight of width option * bv_exp * bv_exp
(*
  | BComp of bv_exp * bv_exp
*)
  | ZeroExtend of width option * width * bv_exp

type bv_rel =
  | Eq of bv_exp * bv_exp
  | ULt of bv_exp * bv_exp
  | ULeq of bv_exp * bv_exp
  | UGt of bv_exp * bv_exp
  | UGeq of bv_exp * bv_exp

type bv_formula =
  | Atom of bv_rel
  | And of bv_formula list
  | Or of bv_formula list
  | Not of bv_formula
  | True
  | False

let rec exponentiate (x : int) (p : int) : int =
  assert (p >= 0);
  begin
    match p with
    | 0 -> 1
    | 1 -> x
    | _ -> x * exponentiate x (p - 1)
  end

let rec string_of_bv_exp (bv_e : bv_exp) : string =
  match bv_e with
  | Hole (_, s) -> s
  | Const (Some w, n) ->
      assert (exponentiate 2 w > n);
      let nibble_width =
        let pre_nibble_width = float_of_int w /. 4. in
        (*width must be divisible by 4*)
        assert(pre_nibble_width = ceil(pre_nibble_width));
        int_of_float pre_nibble_width in
      begin
        "(_ bv" ^ string_of_int n ^ " " ^ string_of_int w ^ ")"
      end
  | Concat (_, bv_e1, bv_e2) ->
      "(concat " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | Extract (o, w, bv_e') ->
      "((_ extract " ^
      string_of_int o ^ " " ^
      string_of_int w ^ ") " ^
      string_of_bv_exp bv_e' ^
      ")"
  | BNot (_, bv_e') ->
      "(bvnot " ^
      string_of_bv_exp bv_e' ^
      ")"
  | BAnd (_, bv_e1, bv_e2) ->
      "(bvand " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | BOr (_, bv_e1, bv_e2) ->
      "(bvor " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | BXor (_, bv_e1, bv_e2) ->
      "(bvxor " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | BNegate (_, bv_e') ->
      "(bvneg " ^
      string_of_bv_exp bv_e' ^
      ")"
  | BPlus (_, bv_e1, bv_e2) ->
      "(bvadd " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | BMul (_, bv_e1, bv_e2) ->
      "(bvmul " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | BDiv (_, bv_e1, bv_e2) ->
      "(bvudiv " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | BRem (_, bv_e1, bv_e2) ->
      "(bvurem " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | BMod (_, bv_e1, bv_e2) ->
      "(bvumod " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | BMinus (_, bv_e1, bv_e2) ->
      "(bvsub " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | ShLeft (_, bv_e1, bv_e2) ->
      "(bvshl " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | LShRight (_, bv_e1, bv_e2) ->
      "(bvlshr " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
(*
  | BComp (bv_e1, bv_e2) ->
      of bv_exp * bv_exp
*)
  | ZeroExtend  (_, w, bv_e') ->
      "((_ zero_extend " ^
      string_of_int w ^ ") " ^
      string_of_bv_exp bv_e' ^
      ")"

let string_of_bv_rel (bv_r : bv_rel) : string =
  match bv_r with
  | Eq (bv_e1, bv_e2) ->
      "(= " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | ULt (bv_e1, bv_e2) ->
      "(bvult " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | ULeq (bv_e1, bv_e2) ->
      "(bvule " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | UGt (bv_e1, bv_e2) ->
      "(bvugt " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"
  | UGeq (bv_e1, bv_e2) ->
      "(bvuge " ^
      string_of_bv_exp bv_e1 ^
      " " ^
      string_of_bv_exp bv_e2 ^
      ")"

let rec string_of_bv_formula (bv_f : bv_formula) : string =
  match bv_f with
  | Atom bv_r -> string_of_bv_rel bv_r
  | And bv_fs ->
      "(and " ^
      (List.map string_of_bv_formula bv_fs
       |> String.concat " ") ^
      ")"
  | Or bv_fs ->
      "(or " ^
      (List.map string_of_bv_formula bv_fs
       |> String.concat " ") ^
      ")"
  | Not bv_f' ->
      "(not " ^
      string_of_bv_formula bv_f' ^
      ")"
  | True -> "true"
  | False -> "false"
