#!/bin/bash
# Caper: a pcap expression analysis utility.
# Nik Sultana, February 2023
#
# Example command to update expected results for a test:
#   ./compare_test.sh D19:

# FIXME sensitive to the directory in which this is run

SELECTOR=$1

echo "SELECTOR=${SELECTOR}"
echo "From current tests:"
grep "${SELECTOR}" <(./run_tests.sh dontcompare)
echo
echo "From expected results:"
grep "${SELECTOR}" tests/*.results
