(*
  Copyright Nik Sultana, March 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Rewriting a pcap expression to reorder subformulas to start with
  those that involve predicates on lower layers.
*)

open Pcap_syntax
open Pcap_syntax_aux


let is_numeric (s : string) : bool =
  (*FIXME crude way of checking if string is a numeral*)
  try
    int_of_string s;
    true
  with Failure("int_of_string") -> false

let rec pe_le_than (invert : bool) (pe1 : pcap_expression) (pe2 : pcap_expression) : bool =
  if pe1 = pe2 then true
  else
    let do_inversion (b : bool) : bool = if invert then not b else b in
    match pe1, pe2 with
    | True, _
    | False, _
    | _, True
    | _, False -> true (*Don't care about the order since these constants will be purged. No need to apply do_inversion.*)

    | Atom re, Primitive ((Some proto2, _, _), _) ->
        let protos =
          Names.protos_in_re re Names.StringSet.empty
          |> Names.protos_of_strings in
        Names.ProtoSet.fold (fun proto acc ->
          if false = acc then acc
          else if Layering.layer_of_proto proto < Layering.layer_of_proto proto2 then true
          else if Layering.layer_of_proto proto > Layering.layer_of_proto proto2 then false
          else
           begin
             assert (Layering.layer_of_proto proto = Layering.layer_of_proto proto2);
             let comparison = String.compare (string_of_proto proto) (string_of_proto proto2) in
             comparison < 0
           end
        ) protos true
        |> do_inversion

    | Primitive ((Some proto1, _, typ1), v1), Primitive ((Some proto2, _, typ2), v2) ->
        if Layering.layer_of_proto proto1 < Layering.layer_of_proto proto2 then
          do_inversion true
        else if Layering.layer_of_proto proto1 > Layering.layer_of_proto proto2 then
          do_inversion false
        else (*proto1 = proto2*)
          let compare_values v1 v2 =
            begin
              (*NOTE i don't compare typ1 and typ2 any further, maybe that should be attempted first*)
              match v1, v2 with
              | String s1, String s2
              | Escaped_String s1, Escaped_String s2
              | MAC_Address s1, MAC_Address s2 ->
                  do_inversion (s1 < s2)
              | IPv4_Address (i1_1, i1_2, i1_3, i1_4), IPv4_Address (i2_1, i2_2, i2_3, i2_4) ->
                  if i1_1 = i2_1 then
                    if i1_2 = i2_2 then
                      if i1_3 = i2_3 then do_inversion (i1_4 < i2_4)
                      else do_inversion (i1_3 < i2_3)
                    else do_inversion (i1_2 < i2_2)
                  else do_inversion (i1_1 < i2_1)
              | IPv4_Network (i1_1, i1_2, i1_3, i1_4, m1), IPv4_Network (i2_1, i2_2, i2_3, i2_4, m2) ->
                  if i1_1 = i2_1 then
                    if i1_2 = i2_2 then
                      if i1_3 = i2_3 then
                        if i1_4 = i2_4 then do_inversion (m1 < m2)
                        else do_inversion (i1_4 < i2_4)
                      else do_inversion (i1_3 < i2_3)
                    else do_inversion (i1_2 < i2_2)
                  else do_inversion (i1_1 < i2_1)
              | IPv4_Network_Masked (i1_1, i1_2, i1_3, i1_4, m1), IPv4_Network_Masked (i2_1, i2_2, i2_3, i2_4, m2) ->
                  if i1_1 = i2_1 then
                    if i1_2 = i2_2 then
                      if i1_3 = i2_3 then
                        if i1_4 = i2_4 then do_inversion (m1 < m2)
                        else do_inversion (i1_4 < i2_4)
                      else do_inversion (i1_3 < i2_3)
                    else do_inversion (i1_2 < i2_2)
                  else do_inversion (i1_1 < i2_1)
              | IPv6_Network (s1_1, s1_2, s1_3), IPv6_Network (s2_1, s2_2, s2_3) ->
                  if s1_1 = s2_1 then
                    if s1_2 = s2_2 then do_inversion (s1_3 < s2_3)
                    else do_inversion (s1_2 < s2_2)
                  else do_inversion (s1_1 < s2_1)
              | Port s1, Port s2 ->
                  if is_numeric s1 && is_numeric s2 then
                    do_inversion (int_of_string s1 < int_of_string s2)
                  else do_inversion (s1 < s2)
              | IPv6_Address (s1_1, s1_2), IPv6_Address (s2_1, s2_2) ->
                  if s1_1 = s2_1 then do_inversion (s1_2 < s2_2)
                  else do_inversion (s1_1 < s2_1)
              | Port_Range (s1_1, s1_2), Port_Range (s2_1, s2_2) ->
                  if s1_1 = s2_1 then
                    if is_numeric s1_2 && is_numeric s2_2 then
                      do_inversion (int_of_string s1_2 < int_of_string s2_2)
                    else
                      do_inversion (s1_2 < s2_2)
                  else
                    if is_numeric s1_1 && is_numeric s2_1 then
                      do_inversion (int_of_string s1_1 < int_of_string s2_1)
                    else
                      do_inversion (s1_1 < s2_1)
              | _, _ ->
                (*FIXME arbitrary choice being made here*)
                do_inversion true
            end in
          begin
            assert (Layering.layer_of_proto proto1 = Layering.layer_of_proto proto1);
            let comparison = String.compare (string_of_proto proto1) (string_of_proto proto2) in
            if comparison < 0 then do_inversion true
            else if comparison > 0 then do_inversion false
            else
              match typ1, typ2 with
              | Some Proto, Some Proto ->
                  compare_values v1 v2
              | Some Proto, _ ->
                  do_inversion true
              | _, Some Proto ->
                  do_inversion false
              | _, _ ->
                  compare_values v1 v2
          end

    | Primitive ((None, Some Inbound, None), Nothing), Primitive ((_, dir, _), _) when dir <> Some Outbound ->
        do_inversion true
    | Primitive ((None, Some Outbound, None), Nothing), Primitive ((_, dir, _), _) when dir <> Some Inbound ->
        do_inversion true

    | Primitive ((_, dir, _), _), Primitive ((None, Some Inbound, None), Nothing) when dir <> Some Outbound ->
        do_inversion false
    | Primitive ((_, dir, _), _), Primitive ((None, Some Outbound, None), Nothing) when dir <> Some Inbound ->
        do_inversion false

    | Primitive _, Primitive _ ->
        (*This case should not be possible, since protocols should have been made explicit by expansion.*)
        failwith (Aux.error_output_prefix ^ ": pe_leq_than: " ^ "Impossible")

    | Atom re1, Atom re2 ->
        let protos1 =
          Names.protos_in_re re1 Names.StringSet.empty
          |> Names.protos_of_strings in
        let protos2 =
          Names.protos_in_re re2 Names.StringSet.empty
          |> Names.protos_of_strings in
        let less_than =
          Names.ProtoSet.fold (fun proto1 acc ->
            if false = acc then acc
            else
              Names.ProtoSet.fold (fun proto2 acc ->
                if false = acc then acc
                (*FIXME DRY from earlier case*)
                else if Layering.layer_of_proto proto1 < Layering.layer_of_proto proto2 then true
                else if Layering.layer_of_proto proto1 > Layering.layer_of_proto proto2 then false
                else
                 begin
                   assert (Layering.layer_of_proto proto1 = Layering.layer_of_proto proto2);
                   let comparison = String.compare (string_of_proto proto1) (string_of_proto proto2) in
                   comparison < 0
                 end
              ) protos2 acc
          ) protos1 true in
        do_inversion less_than

    | Primitive _, And pes
    | Primitive _, Or pes
    | Atom _, And pes
    | Atom _, Or pes ->
        pe_le_than invert pe1 (List.hd pes) (*NOTE assuming pes to be ordered*)

    | Primitive _, Not pe2'
    | Atom _, Not pe2' ->
        pe_le_than invert pe1 pe2'
    | And pes, Not _
    | Or pes, Not _ ->
        pe_le_than invert (List.hd(*NOTE assuming pes to be ordered*) pes) pe2

    | And pes1, And pes2
    | Or pes1, Or pes2
    | And pes1, Or pes2
    | Or pes1, And pes2 ->
        if pes1 = [] || pes2 = [] then do_inversion false
        else
          begin
            assert(pes1 <> [] && pes2 <> []);
            if List.hd pes1 = List.hd pes2 then pe_le_than invert (And(*Connective doesn't matter for this*) (List.tl pes1)) (And(*Connective doesn't matter for this*) (List.tl pes2))
            else pe_le_than invert (List.hd pes1) (List.hd pes2)
          end

    | Not pe1', Not pe2' -> pe_le_than invert pe1' pe2'

    | And _, _
    | Or _, _
    | Not _, _
    | Primitive _, Atom _ -> pe_le_than (not invert) pe2 pe1

    | _, _ ->
        failwith ("pe_le_than: Unable to compare '" ^ Pcap_syntax_aux.string_of_pcap_expression pe1 ^ "' and '" ^ Pcap_syntax_aux.string_of_pcap_expression pe2 ^ "'\n")


let rec reorder_subformulas (pe : pcap_expression) : pcap_expression =
    match pe with
    | True
    | False
    | Primitive _
    | Atom _ -> pe
    | Not pe -> Not (reorder_subformulas pe)
    | And pes
    | Or pes ->
        begin
        (*I split up the processing like this to avoid having the processing
          stay only in the even strides of "pe1 :: pe2 :: " below. This enables
          me  to analyse more widely. I do the same thing in the OR case next.*)
        let tail = order_primitives (List.map reorder_subformulas (List.tl pes)) in
        let pes' = order_primitives (reorder_subformulas (List.hd pes) :: tail) in
        match pe with
        | And _ -> And pes'
        | Or _ -> Or pes'
        | _ -> failwith "Impossible"
        end
and order_primitives (pes : pcap_expression list) : pcap_expression list =
  match pes with
  | []
  | [_] -> List.map reorder_subformulas pes
  | pe1 :: pe2 :: rest ->
      if pe_le_than false pe1 pe2 then
        reorder_subformulas pe1 :: reorder_subformulas pe2 :: order_primitives rest
      else
        reorder_subformulas pe2 :: reorder_subformulas pe1 :: order_primitives rest
