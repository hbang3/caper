#!/bin/bash
# Caper: a pcap expression analysis utility.
# Run regression tests wrt syntax
# Nik Sultana, August 2015.

# FIXME sensitive to the directory in which this is run
tests/syntax_regression.sh "./caper.byte -1stdin -not_expand -q" tests/syntax_test.sh
