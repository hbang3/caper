#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for Pcap to English
# Marelle Leon, April 2023

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=$(basename "$0")

function tst() {
  if [ "$#" -ne 3 ]; then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$3" "$1"
}

tst "E1" "src foo" "the source of some header is foo"

tst "E2" "host foo" "the host of some header is foo"

tst "E3" "src host foo" "the source host of some header is foo"

tst "E4" "ip" "protocol is IPv4"

tst "E5" "ip src foo" "the source of the IPv4 header is foo"

tst "E6" "ip host foo" "the host of the IPv4 header is foo"

tst "E7" "ip src host foo" "the source host of the IPv4 header is foo"

tst "E8" "! src foo" "the source of some header is not foo"

tst "E9" "! host foo" "the host of some header is not foo"

tst "E10" "! src host foo" "the source host of some header is not foo"

tst "E11" "! ip src foo" "the source of the IPv4 header is not foo"

tst "E12" "! ip host foo" "the host of the IPv4 header is not foo"

tst "E13" "! ip src host foo" "the source host of the IPv4 header is not foo"

tst "E14" "port ftp-data" "the port of some header is ftp-data"

tst "E15" "dst port ftp-data" "the destination port of some header is ftp-data"

tst "E16" "tcp" "protocol is tcp"

tst "E17" "inbound" "direction is inbound"

tst "E18" "tcp port ftp-data" "the port of the tcp header is ftp-data"

tst "E19" "tcp dst port ftp-data" "the destination port of the tcp header is ftp-data"

tst "E20" "portrange 6000-6008" "the port range of some header is 6000-6008"

tst "E21" "net 128.3" "the network of some header is 128.3"

tst "E22" "port 20" "the port of some header is 20"

tst "E23" "src or dst port ftp-data" "the source or destination port of some header is ftp-data"

tst "E24" "ether src foo" "the source of the ethernet header is foo"

tst "E25" "arp net 128.3" "the network of the arp header is 128.3"

tst "E26" "tcp port 21" "the port of the tcp header is 21"

tst "E27" "udp portrange 7000-7009" "the port range of the udp header is 7000-7009"

tst "E28" "ether host 00:02:03:04:05:06" "the host of the ethernet header is 00:02:03:04:05:06"

tst "E29" "ip src foo || arp src foo || rarp src foo" "the source of the IPv4 header is foo or (the source of the arp header is foo or the source of the rarp header is foo)"

tst "E30" "ip net bar || arp net bar || rarp net bar" "the network of the IPv4 header is bar or (the network of the arp header is bar or the network of the rarp header is bar)"

tst "E31" "tcp port 53 || udp port 53" "the port of the tcp header is 53 or the port of the udp header is 53"

tst "E32" "host foo && ! port ftp && ! port ftp-data" "the host of some header is foo and (the port of some header is not ftp and the port of some header is not ftp-data)"

tst "E33" "host \\host" "the host of some header is host"

tst "E34" "src host \\host" "the source host of some header is host"

tst "E35" "ip host \\host" "the host of the IPv4 header is host"

tst "E36" "ip src host \\host" "the source host of the IPv4 header is host"

tst "E37" "ip[0] = 1" "the 0th byte of the IPv4 header is equal to { 1 }"

tst "E38" "tcp[0] = 1" "the 0th byte of the tcp header is equal to { 1 }"

tst "E39" "ip[1] = 1" "the first byte of the IPv4 header is equal to { 1 }"

tst "E40" "ip[0 : 2] = 1" "the 0th 2 bytes of the IPv4 header is equal to { 1 }"

tst "E41" "ip[0 : 4] = 1" "the 0th 4 bytes of the IPv4 header is equal to { 1 }"

tst "E42" "ip[0] = (0 + 1)" "the 0th byte of the IPv4 header is equal to { 0 + 1 }"

tst "E43" "ip[0] = (ip[0] | (0 + 1))" "examining the 0th byte of the IPv4 header: { # it } is equal to { # it | (0 + 1) }"

tst "E44" "ip[0] = ((0 + 1) | ip[0])" "examining the 0th byte of the IPv4 header: { # it } is equal to { (0 + 1) | # it }"

tst "E45" "ip[0] = ((ip[0] + 1) | 1)" "examining the 0th byte of the IPv4 header: { # it } is equal to { (# it + 1) | 1 }"

tst "E46" "ip[0] = ((1 + ip[0]) | 1)" "examining the 0th byte of the IPv4 header: { # it } is equal to { (1 + # it) | 1 }"

tst "E47" "ether proto \ip && host \host" "the proto of the ethernet header is IPv4 and the host of some header is host"

tst "E48" "(tcp[tcpflags] & (tcp-syn | tcp-fin)) != 0" "{ tcp[tcpflags] & (tcp-syn | tcp-fin) } does not equal { 0 }"

tst "E49" "tcp dst port ftp || ftp-data || domain" "the destination port of the tcp header is one of [ftp, ftp-data, domain]"

tst "E50" "port ftp || ftp-data || domain" "the port of some header is one of [ftp, ftp-data, domain]"

tst "E51" "! (port ftp || ftp-data)" "the port of some header is not one of [ftp, ftp-data]"

tst "E52" "host 0:0:0:0:0:0:0:1" "the host of some header is 0:0:0:0:0:0:0:1"

tst "E53" "gateway 1" "the gateway of some header is 1"

tst "E54" "dst net 172.16 mask 255.255.0.0" "the destination network of some header is 172.16 with a mask of 255.255.0.0"

tst "E55" "! dst net 172.16 mask 255.255.0.0" "the destination network of some header is not 172.16 with a mask of 255.255.0.0"

tst "E56" "dst net 172.16/16" "the destination network of some header is 172.16 of length 16 bits"

tst "E57" "dst net 10/8" "the destination network of some header is 10 of length 8 bits"

tst "E58" "ip broadcast" "IPv4 that is broadcast"

tst "E59" "icmp || udp port 53 || bootpc" "protocol is icmp or the port of the udp header is one of [53, bootpc]"

tst "E60" "ip && (udp[8 : 2] = 0x1111) || (icmp[8 : 2] = 0x1111) || (tcp[((tcp[12] & 0xf0) >> 2) : 2] = 0x1111)" "(protocol is IPv4 and the eighth 2 bytes of the udp header is equal to { 0x1111 }) or (the eighth 2 bytes of the icmp header is equal to { 0x1111 } or { tcp[(tcp[12] & 0xf0) >> 2 : 2] } is equal to { 0x1111 })"

tst "E61" "(ip && udp[8 : 2] = 0x1111) || (icmp[8 : 2] = 0x1111 || tcp[(tcp[12] & 0xf0) >> 2 : 2] = 0x1111)" "(protocol is IPv4 and the eighth 2 bytes of the udp header is equal to { 0x1111 }) or (the eighth 2 bytes of the icmp header is equal to { 0x1111 } or { tcp[(tcp[12] & 0xf0) >> 2 : 2] } is equal to { 0x1111 })"

tst "E62" "tcp port 80 && ((ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2) != 0" "the port of the tcp header is 80 and { (# the total length of the IPv4 header - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 } does not equal { 0 }"

tst "E63" "ip6 dst host fe80:0000:0000:0000:1111:11ff:fe11:1111" "the destination host of the IPv6 header is fe80:0000:0000:0000:1111:11ff:fe11:1111"

tst "E64" "port 80 && tcp[((tcp[12:1] & 0xf0) >> 2) : 4] = 0x12345678" "the port of some header is 80 and { tcp[(tcp[12 : 1] & 0xf0) >> 2 : 4] } is equal to { 0x12345678 }"

tst "E65" "(tcp[tcp-flags] & tcp-fin) = tcp-fin" "tcp with flag FIN"

tst "E66" "(tcp[tcp-flags] & tcp-syn) = tcp-syn" "tcp with flag SYN"

tst "E67" "ip[9] = 0x37" "IPv4 with protocol 0x37"

tst "E68" "ip6[6] = 0x37" "IPv6 with nextHeader 0x37"

tst "E69" "ip[9] < 0x37" "the protocol of the IPv4 header is less than { 0x37 }"

tst "E70" "ip6[6] < 0x37" "the nextHeader of the IPv6 header is less than { 0x37 }"

tst "E71" "tcp[tcp-flags] & tcp-syn < 20" "the SYN of the tcp header is less than { 20 }"

tst "E72" "tcp[tcp-flags] & (1 << 7) < 20" "the CWR of the tcp header is less than { 20 }"

tst "E73" "tcp[13] & (1 << 7) < 20" "the CWR of the tcp header is less than { 20 }"

tst "E74" "tcp[0 : 2] < 81" "the source port of the tcp header is less than { 81 }"

tst "E75" "tcp[2 : 2] < 81" "the destination port of the tcp header is less than { 81 }"

tst "E76" "ip[12 : 4] > 0" "the source host of the IPv4 header is greater than { 0 }"

tst "E77" "ip[16 : 4] > 0" "the destination host of the IPv4 header is greater than { 0 }"

tst "E78" "! ip" "protocol is not IPv4"
