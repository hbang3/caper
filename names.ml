(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Name resolution for pcap expressions
*)

open Pcap_syntax
open Pcap_syntax_aux

(** Map names to numbers **)

let rec resolve_pe (e : pcap_expression) : pcap_expression =
  match e with
  | Primitive (f, v) ->
      begin
      match v with
      | Port s ->
          begin
            if String_features.is_unsignedint s then e
            else
              let s' =
                match s with
                | "ftp" -> "21"
                | "ftp-data" -> "22"
                | "domain" -> "53"
                | "http" -> "80"
                | "bootpc" -> "68"
                | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "Unrecognised port name: " ^ s) in
              Primitive (f, Port s')
          end
      | _ -> e
      end
  | True
  | False -> e
  | Atom re -> Atom (resolve_re re)
  | Not e' -> Not (resolve_pe e')
  | And pes -> And (List.map resolve_pe pes)
  | Or pes -> Or (List.map resolve_pe pes)

and resolve_e (e : expr) : expr =
  match e with
  | Identifier s ->
      begin
        match s with
        | "tcp-fin" -> Hex_Literal "0x01"
        | "tcp-syn" -> Hex_Literal "0x02"
        | "tcp-rst" -> Hex_Literal "0x04"
        | "tcp-push" -> Hex_Literal "0x08"
        | "tcp-ack" -> Hex_Literal "0x10"
        | "tcp-urg" -> Hex_Literal "0x20"
        | "icmp-echoreply" -> Nat_Literal 0
        | "icmp-unreach" -> Nat_Literal 3
        | "icmp-sourcequench" -> Nat_Literal 4
        | "icmp-redirect" -> Nat_Literal 5
        | "icmp-echo" -> Nat_Literal 8
        | "icmp-routeradvert" -> Nat_Literal 9
        | "icmp-routersolicit" -> Nat_Literal 10
        | "icmp-timxceed" -> Nat_Literal 11
        | "icmp-paramprob" -> Nat_Literal 12
        | "icmp-tstamp" -> Nat_Literal 13
        | "icmp-tstampreply" -> Nat_Literal 14
        | "icmp-ireq" -> Nat_Literal 15
        | "icmp-ireqreply" -> Nat_Literal 16
        | "icmp-maskreq" -> Nat_Literal 17
        | "icmp-maskreply" -> Nat_Literal 18
        | "icmp6-destinationunreach" -> Nat_Literal 1
        | "icmp6-packettoobig" -> Nat_Literal 2
        | "icmp6-timeexceeded" -> Nat_Literal 3
        | "icmp6-parameterproblem" -> Nat_Literal 4
        | "icmp6-echo" -> Nat_Literal 128
        | "icmp6-echoreply" -> Nat_Literal 129
        | "icmp6-multicastlistenerquery" -> Nat_Literal 130
        | "icmp6-multicastlistenerreportv1" -> Nat_Literal 131
        | "icmp6-multicastlistenerdone" -> Nat_Literal 132
        | "icmp6-routersolicit" -> Nat_Literal 133
        | "icmp6-routeradvert" -> Nat_Literal 134
        | "icmp6-neighborsolicit" -> Nat_Literal 135
        | "icmp6-neighboradvert" -> Nat_Literal 136
        | "icmp6-redirect" -> Nat_Literal 137
        | "icmp6-routerrenum" -> Nat_Literal 138
        | "icmp6-nodeinformationquery" -> Nat_Literal 139
        | "icmp6-nodeinformationresponse" -> Nat_Literal 140
        | "icmp6-ineighbordiscoverysolicit" -> Nat_Literal 141
        | "icmp6-ineighbordiscoveryadvert" -> Nat_Literal 142
        | "icmp6-multicastlistenerreportv2" -> Nat_Literal 143
        | "icmp6-homeagentdiscoveryrequest" -> Nat_Literal 144
        | "icmp6-homeagentdiscoveryreply" -> Nat_Literal 145
        | "icmp6-mobileprefixsolicit" -> Nat_Literal 146
        | "icmp6-mobileprefixadvert" -> Nat_Literal 147
        | "icmp6-certpathsolicit" -> Nat_Literal 148
        | "icmp6-certpathadvert" -> Nat_Literal 149
        | "icmp6-multicastrouteradvert" -> Nat_Literal 151
        | "icmp6-multicastroutersolicit" -> Nat_Literal 152
        | "icmp6-multicastrouterterm" -> Nat_Literal 153
        | _ -> failwith (Aux.error_output_prefix ^ ": " ^ "Unrecognised name: " ^ s)
      end
  | Packet_Access (proto_name, e', None) ->
      begin
       let resolution =
          match proto_name, e' with
          | "tcp", Identifier "tcpflags" ->
              Some (Nat_Literal 13)
          | "icmp", Identifier "icmptype" ->
              Some (Nat_Literal 0)
          | "icmp", Identifier "icmpcode" ->
              Some (Nat_Literal 1)
          | _ -> None in
       begin
         match resolution with
         | None -> e
         | Some resolution ->
           Aux.diag_output ("resolve_e " ^
             Pcap_syntax_aux.string_of_expr e' ^
             " --> " ^
             Pcap_syntax_aux.string_of_expr resolution ^
             " in " ^
             Pcap_syntax_aux.string_of_expr e ^ "\n");
           Packet_Access (proto_name, resolution, None)
       end
      end
  | Packet_Access (_, _, Some _) ->
      (*FIXME if we specify width (i.e., Some _) then we
              currently don't do name resolution on other components*)
      e
  | Nat_Literal _
  | Hex_Literal _
  | Len -> e
  | Plus es -> Plus (List.map resolve_e es)
  | Times es -> Times (List.map resolve_e es)
  | Binary_And es -> Binary_And (List.map resolve_e es)
  | Binary_Or es -> Binary_Or (List.map resolve_e es)
  | Binary_Xor es -> Binary_Xor (List.map resolve_e es)
  | Minus (e1, e2) -> Minus (resolve_e e1, resolve_e e2)
  | Quotient (e1, e2) -> Quotient (resolve_e e1, resolve_e e2)
  | Mod (e1, e2) -> Mod (resolve_e e1, resolve_e e2)
  | Shift_Right (e1, e2) -> Shift_Right (resolve_e e1, resolve_e e2)
  | Shift_Left (e1, e2) -> Shift_Left (resolve_e e1, resolve_e e2)

and resolve_re (re : rel_expr) : rel_expr =
  match re with
  | Gt (e1, e2) -> Gt (resolve_e e1, resolve_e e2)
  | Lt (e1, e2) -> Lt (resolve_e e1, resolve_e e2)
  | Geq (e1, e2) -> Geq (resolve_e e1, resolve_e e2)
  | Leq (e1, e2) -> Leq (resolve_e e1, resolve_e e2)
  | Eq (e1, e2) -> Eq (resolve_e e1, resolve_e e2)
  | Neq (e1, e2) -> Neq (resolve_e e1, resolve_e e2)


(** Extract protocols mentioned in an expression **)

module StringSet = Set.Make(String);;

let rec protos_in_pe ?comprehensive:(comprehensive = true) (pe : pcap_expression) (acc : StringSet.t) : StringSet.t =
  match pe with
  | Primitive ((Some proto1, _, Some Proto), Escaped_String proto2_s) ->
      StringSet.add (string_of_proto proto1) acc
      |> (fun set -> if comprehensive then StringSet.add proto2_s set else set)
  | Primitive ((Some proto, _, _), _) ->
      StringSet.add (string_of_proto proto) acc
  | Primitive ((None, _, _), _)
  | True
  | False -> acc
  | Atom re -> protos_in_re re acc
  | Not e' -> protos_in_pe ~comprehensive e' acc
  | And pes
  | Or pes -> List.fold_right (fun pe acc -> protos_in_pe ~comprehensive pe acc) pes acc

and protos_in_e (e : expr) (acc : StringSet.t) : StringSet.t =
  match e with
  | Packet_Access (proto_name, e', _) ->
      StringSet.add proto_name acc
      |> protos_in_e e'
  | Identifier _
  | Nat_Literal _
  | Hex_Literal _
  | Len -> acc
  | Plus es
  | Times es
  | Binary_And es
  | Binary_Or es
  | Binary_Xor es ->
       List.fold_right (fun e acc -> protos_in_e e acc) es acc
  | Minus (e1, e2)
  | Quotient (e1, e2)
  | Mod (e1, e2)
  | Shift_Right (e1, e2)
  | Shift_Left (e1, e2) ->
      protos_in_e e1 acc
      |> protos_in_e e2

and protos_in_re (re : rel_expr) (acc : StringSet.t) : StringSet.t =
  match re with
  | Gt (e1, e2)
  | Lt (e1, e2)
  | Geq (e1, e2)
  | Leq (e1, e2)
  | Eq (e1, e2)
  | Neq (e1, e2) ->
      protos_in_e e1 acc
      |> protos_in_e e2

module ProtoSet = Set.Make(
  struct
    let compare proto1 proto2 =
      if proto1 = proto2 then 0
      else -1 (*NOTE we don't actually define an ordering over "proto"*)
    type t = proto
  end)

let protos_of_strings (protos : StringSet.t) : ProtoSet.t =
  StringSet.fold (fun proto_s set ->
    ProtoSet.add (proto_of_string proto_s) set)
  protos ProtoSet.empty
