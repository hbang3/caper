#!/bin/sh -e
# Caper: a pcap expression analysis utility.
# Nik Sultana, November 2018.
#
# When run with CAPER_WITH_ENGLISH=1 then it will also require Angstrom.
#   This Caper feature was tested with Angstrom version 0.15.0.
#   To install, run opam install angstrom.
#   Github: https://github.com/inhabitedtype/angstrom

if ! command -v ocamlbuild >/dev/null; then
  opam init --quiet --safe
  eval "$(opam env)"
fi

TARGET=${1:-caper.byte}

echo "building ${TARGET}"
if [ -n "${CAPER_WITH_ENGLISH}" ]
then
  M4_ARG='-D'
  echo "... with English conversion support"
else
  M4_ARG='-U'
  echo "... without English conversion support"
fi

m4 "${M4_ARG}CAPER_WITH_ENGLISH" caper.ml.m4 > caper.ml
m4 "${M4_ARG}CAPER_WITH_ENGLISH" html.ml.m4 > html.ml

# NOTE could add -dont-catch-errors to have exceptions pass through catches.
ocamlbuild -cflag -g -lflag -g -tag thread -use-ocamlfind -use-menhir \
  -no-hygiene \
  -package str \
  ${CAPER_WITH_ENGLISH:+-package angstrom} \
  -I syntax \
  -I pcap_to_bpf \
  -I pcap_to_bpf/protocol \
  -I pcap_to_bpf/optimizer \
  ${CAPER_WITH_ENGLISH:+-I english} \
  "$TARGET"
