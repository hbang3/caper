(*
  Copyright Nik Sultana, November 2018

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Parser interface
*)

open Lexing
open Pcap_syntax


let gather_input () : string list =
  let inp : string list ref = ref [] in
  try
    while true do
      inp := input_line stdin :: !inp
    done;
    List.rev !inp
  with End_of_file -> List.rev !inp

let print_position outx lexbuf =
  let pos = lexbuf.lex_curr_p in
  Printf.fprintf outx "%s:%d:%d" pos.pos_fname
    pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)

let parse_with_error lexbuf : Pcap_syntax.pcap_expression =
  try Pcap_parser.pcap_main Pcap_lexer.main lexbuf with
  | Pcap_parser.Error ->
    (* FIXME use Aux.error_output *)
    Printf.fprintf stderr "%s: %a: syntax error\n" Aux.error_output_prefix print_position lexbuf;
    False

let parse_string s =
  let lexbuf = Lexing.from_string s in
  lexbuf.lex_curr_p <- lexbuf.lex_curr_p;
  parse_with_error lexbuf