open Inst
open Headers
open Util

let vxlan_type = function
  | Headers.Ip -> 0x800
  | Headers.Ip6 -> 0x86dd
  | Headers.Arp -> 0x806
  | Headers.Rarp -> 0x8035
  | Headers.Mpls -> 0x8847
  | _ -> abort_bpf_gen "unsupported protocol over vxlan"

let vxlan_proto (hdr : header) (pkt : packet) : sock_filter option =
  let base_idxs = get_base_idxs pkt Headers.Vxlan in
  let vxlan_proto_helper idxs =
    match idxs with
    | Ldx (Lit x) ->
      Some {
        code = [
          Ldh (Off (x + 20));
        ];
        cond = Jeq (Hexj (vxlan_type hdr));
        jt = ret_true;
        jf = ret_false;
      }
    | List x ->
      Some {
        code = x @ [
          Ldh (Offset (Exp (X, Lit 20, Addition)));
        ];
        cond = Jeq (Hexj (vxlan_type hdr));
        jt = ret_true;
        jf = ret_false;
      }
    in
    vxlan_proto_helper base_idxs

let vxlan_vni (vni : int) (pkt : packet) : sock_filter option =
  let base_idxs = get_base_idxs pkt Headers.Vxlan in
  let vxlan_vni_helper idxs =
    match idxs with
    | Ldx (Lit x) ->
      Some {
        code = [
          Ld (Off (x + 4));
          And (Hexj 0xffffff00);
        ];
        cond = Jeq (Hexj vni);
        jt = ret_true;
        jf = ret_false;
      }
    | List x ->
      Some {
        code = x @ [
          Ld (Offset (Exp (X, Lit 4, Addition)));
          And (Hexj 0xffffff00);
        ];
        cond = Jeq (Hexj vni);
        jt = ret_true;
        jf = ret_false;
      }
    in
    vxlan_vni_helper base_idxs

let vxlan_to_sock_filter (vxlan_info : string list) (pkt : packet) : sock_filter option =
  match vxlan_info with
  | "proto" :: [protocol] -> vxlan_proto (string_to_header protocol) pkt
  | [vni] -> vxlan_vni (int_of_string vni) pkt