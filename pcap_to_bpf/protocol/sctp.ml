(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: sctp
*)

open Port
open Portrange
open Util
open Inst
open Headers

let sctp_to_sock_filter (sctp_info : string list) (pkt : packet) : sock_filter option =
  match sctp_info with
  | "src" :: "and" :: "dst" :: "port" :: [port_num] -> src_dst_port "and" Headers.Sctp port_num pkt
  | "src" :: "or" :: "dst" :: "port" :: [port_num] -> src_dst_port "or" Headers.Sctp port_num pkt
  | "src" :: "port" :: [port_num] -> src_port Headers.Sctp port_num pkt
  | "dst" :: "port" :: [port_num] -> dst_port Headers.Sctp port_num pkt
  | "src" :: "and" :: "dst" :: "portrange" :: [portrange] -> src_dst_portrange "and" Headers.Sctp portrange pkt
  | "src" :: "or" :: "dst" :: "portrange" :: [portrange] -> src_dst_portrange "or" Headers.Sctp portrange pkt
  | "src" :: "portrange" :: [portrange] -> src_portrange Headers.Sctp portrange pkt
  | "dst" :: "portrange" :: [portrange] -> dst_portrange Headers.Sctp portrange pkt
  | _ -> abort_bpf_gen "Sctp.sctp_to_sock_filter"