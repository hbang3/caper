(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: host handler for ip, ip6, arp, and rarp protocol
*)

open Inst
open Util
open Headers

let src_host (hdr : header) (host_num : string) (pkt : packet): sock_filter option =
  match hdr with
  | Headers.Ip | Headers.Arp | Headers.Rarp ->
    let host = String.split_on_char '.' host_num in
    if List.length host == 1 && is_digit (List.hd host) then
      Some {
        code = [
          Ld (Off (sum_of_header pkt hdr + src_ip_offset hdr))
        ];
        cond = Jeq (Hexj (int_of_string (List.hd host)));
        jt = ret_true;
        jf = ret_false;
      }
    else if List.length host == 4 then
      Some {
        code = [
          Ld (Off (sum_of_header pkt hdr + src_ip_offset hdr))
        ];
        cond = Jeq (Hexj (ipv4_to_lit host));
        jt = ret_true;
        jf = ret_false;
      }
    else
      abort_bpf_gen "invalid ip host (Host.src_host)"
  | Headers.Ip6 ->
    let host = String.split_on_char ':' host_num in
    let ipv6_1 = Some {
      code = [
        Ld (Off (sum_of_header pkt hdr + src_ip_offset hdr))
      ];
      cond = Jeq (Hexj (ipv6_piece_to_lit host 0));
      jt = ret_true;
      jf = ret_false;
    }
    in
    let ipv6_2 = Some {
      code = [
        Ld (Off (sum_of_header pkt hdr + src_ip_offset hdr + 4))
      ];
      cond = Jeq (Hexj (ipv6_piece_to_lit host 1));
      jt = ret_true;
      jf = ret_false;}
    in
    let ipv6_3 = Some {
      code = [
        Ld (Off (sum_of_header pkt hdr + src_ip_offset hdr + 8))
      ];
      cond = Jeq (Hexj (ipv6_piece_to_lit host 2));
      jt = ret_true;
      jf = ret_false;
    } in
    let ipv6_4 = Some {
      code = [
        Ld (Off (sum_of_header pkt hdr + src_ip_offset hdr + 12))
      ];
      cond = Jeq (Hexj (ipv6_piece_to_lit host 3));
      jt = ret_true;
      jf = ret_false;}
    in
    conjoin ipv6_1 (conjoin ipv6_2 (conjoin ipv6_3 ipv6_4))
  | _ -> abort_bpf_gen "Host must be used with ip or ip6 (Host.src_host)"

let dst_host (hdr : header) (host_num : string) (pkt : packet) : sock_filter option =
  match hdr with
  | Headers.Ip | Headers.Arp | Headers.Rarp ->
    let host = String.split_on_char '.' host_num in
    if List.length host == 1 && is_digit (List.hd host) then
      Some {
        code = [
          Ld (Off (sum_of_header pkt hdr + dst_ip_offset hdr))
        ];
        cond = Jeq (Hexj (int_of_string (List.hd host)));
        jt = ret_true;
        jf = ret_false;
      }
    else if List.length host == 4 then
      Some {
        code = [
          Ld (Off (sum_of_header pkt hdr + dst_ip_offset hdr))
        ];
        cond = Jeq (Hexj (ipv4_to_lit host));
        jt = ret_true;
        jf = ret_false;
      }
    else
      abort_bpf_gen "invalid ipv4 host (Host.dst_host)"
  | Headers.Ip6 ->
    let host = String.split_on_char ':' host_num in
    let ipv6_1 = Some {
      code = [
        Ld (Off (sum_of_header pkt hdr + dst_ip_offset hdr))
      ];
      cond = Jeq (Hexj (ipv6_piece_to_lit host 0));
      jt = ret_true;
      jf = ret_false;
    }
    in
    let ipv6_2 = Some {
      code = [
        Ld (Off (sum_of_header pkt hdr + dst_ip_offset hdr + 4))
      ];
      cond = Jeq (Hexj (ipv6_piece_to_lit host 1));
      jt = ret_true;
      jf = ret_false;}
    in
    let ipv6_3 = Some {
      code = [
        Ld (Off (sum_of_header pkt hdr + dst_ip_offset hdr + 8))
      ];
      cond = Jeq (Hexj (ipv6_piece_to_lit host 2));
      jt = ret_true;
      jf = ret_false;
    } in
    let ipv6_4 = Some {
      code = [
        Ld (Off (sum_of_header pkt hdr + dst_ip_offset hdr + 12))
      ];
      cond = Jeq (Hexj (ipv6_piece_to_lit host 3));
      jt = ret_true;
      jf = ret_false;}
    in
    conjoin ipv6_1 (conjoin ipv6_2 (conjoin ipv6_3 ipv6_4))
  | _ -> abort_bpf_gen "Host must be used with ip or ip6 (Host.dst_host)"
