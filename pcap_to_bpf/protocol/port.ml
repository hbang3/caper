(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: port
*)

open Inst
open Util
open Headers
open Ip6

let resolve_port (port : string) : int =
  match port with
  | "http" -> 0x50
  | "bootpc" -> 0x44
  | "ftp" -> 0x15
  | "ftp-data" -> 0x14
  | other -> int_of_string other

let src_port (hdr : header) (port_num : string) (pkt : packet) : sock_filter option =
  let port = resolve_port port_num in
  match get_l3 pkt with
  | Packet ((Ip6, _), _) ->
    if !Env.generate_protochain then
      Some {
        code = [
          Ldh (Offset (Exp (X, Lit (sum_of_header pkt Headers.Ip6), Addition)))
        ];
        cond = Jeq (Hexj port);
        jt = ret_true;
        jf = ret_false;
      }
    else
      Some {
        code = [
          Ldh (Off (sum_of_header pkt hdr));
        ];
        cond = Jeq (Hexj port);
        jt = ret_true;
        jf = ret_false;
      }

  | Packet ((Ip, _), _) ->
    let ipv4_frag = Some {
      code = [
        Ldh (Off (sum_of_header pkt Headers.Ip + 6))
      ];
      cond = Jset (Hexj 0x1fff);
      jt = ret_false;
      jf = ret_true;
    } in
    let src = Some {
      code = [
        Ldxb (Exp (Lit 4, (Exp (Off (sum_of_header pkt Headers.Ip), Hex 0xf, Arith_and)), Multiplication));
        Ldh (Offset (Exp (X, (Lit (sum_of_header pkt Headers.Ip)), Addition)))];
      cond = Jeq (Hexj (port));
      jt = ret_true;
      jf = ret_false;
    } in
    conjoin ipv4_frag src
  | _ -> abort_bpf_gen "Port.src_port"

let dst_port (hdr : header) (port_num : string) (pkt : packet) : sock_filter option =
  let port = resolve_port port_num in
  match get_l3 pkt with
  | Packet ((Ip6, _), _) ->
    if !Env.generate_protochain then
      Some {
        code = [
          Ldh (Offset (Exp (X, Lit (sum_of_header pkt Headers.Ip6 + 2), Addition)));
        ];
        cond = Jeq (Hexj port);
        jt = ret_true;
        jf = ret_false;
      }
    else
      Some {
        code = [
          Ldh (Off (sum_of_header pkt hdr + 2));
        ];
        cond = Jeq (Hexj port);
        jt = ret_true;
        jf = ret_false;
      }

  | Packet ((Ip, _), _) ->
    let ipv4_frag = Some {
      code = [
        Ldh (Off (sum_of_header pkt Headers.Ip + 6))
      ];
      cond = Jset (Hexj 0x1fff);
      jt = ret_false;
      jf = ret_true;
    } in
    let dst = Some {
      code = [
        Ldxb (Exp (Lit 4, (Exp (Off (sum_of_header pkt Headers.Ip), Hex 0xf, Arith_and)), Multiplication));
        Ldh (Offset (Exp (X, (Lit (sum_of_header pkt Headers.Ip + 2)), Addition)))];
      cond = Jeq (Hexj (port));
      jt = ret_true;
      jf = ret_false;
    } in
    conjoin ipv4_frag dst
  | _ -> abort_bpf_gen "Port.dst_port"

let src_dst_port (bind : string) (hdr : header) (port_num : string) (pkt : packet) : sock_filter option =
  let port = resolve_port port_num in
  match get_l3 pkt with
  | Packet ((Ip6, _), _) ->
    let src =
      if !Env.generate_protochain then
        Some {
          code = [
            Ldh (Offset (Exp (X, Lit (sum_of_header pkt Headers.Ip6), Addition)));
          ];
          cond = Jeq (Hexj port);
          jt = ret_true;
          jf = ret_false;
        }
      else
        Some {
          code = [
            Ldh (Off (sum_of_header pkt hdr));
          ];
          cond = Jeq (Hexj port);
          jt = ret_true;
          jf = ret_false;
        }
    in
    let dst =
      if !Env.generate_protochain then
        Some {
          code = [
            Ldh (Offset (Exp (X, Lit (sum_of_header pkt Headers.Ip6 + 2), Addition)));
          ];
          cond = Jeq (Hexj port);
          jt = ret_true;
          jf = ret_false;
        }
      else
        Some {
          code = [
            Ldh (Off (sum_of_header pkt hdr + 2));
          ];
          cond = Jeq (Hexj port);
          jt = ret_true;
          jf = ret_false;
    } in
    if String.equal bind "and" then conjoin src dst
    else if String.equal bind "or" then disjoin src dst
    else abort_bpf_gen "Port.src_dst_port"

  | Packet ((Ip, _), _) ->
    let ipv4_frag =
      if !Env.generate_protochain then
        nil_sock_filter
      else
        Some {
          code = [
            Ldh (Off (sum_of_header pkt Headers.Ip + 6)) 
          ];
          cond = Jset (Hexj 0x1fff);
          jt = ret_false;
          jf = ret_true;
        }
    in
    let src = Some {
      code = [
        Ldxb (Exp (Lit 4, (Exp ((Off (sum_of_header pkt Headers.Ip)), Hex 0xf, Arith_and)), Multiplication));
        Ldh (Offset (Exp (X, (Lit (sum_of_header pkt Headers.Ip)), Addition)))
      ];
      cond = Jeq (Hexj (port));
      jt = ret_true;
      jf = ret_false;
    } in
    let dst = Some {
      code = [
        Ldxb (Exp (Lit 4, (Exp ((Off (sum_of_header pkt Headers.Ip)), Hex 0xf, Arith_and)), Multiplication));
        Ldh (Offset (Exp (X, (Lit (sum_of_header pkt Headers.Ip + 2)), Addition)))
      ];
      cond = Jeq (Hexj (port));
      jt = ret_true;
      jf = ret_false;
    } in
    if String.equal bind "and" then conjoin ipv4_frag (conjoin src dst)
    else if String.equal bind "or" then conjoin ipv4_frag (disjoin src dst)
    else abort_bpf_gen "Port.src_dst_port"
  | _ -> abort_bpf_gen "Port.src_dst_port"
