open Inst
open Headers
open Util

let gre_type = function
  | Headers.Ip -> 0x800
  | Headers.Ip6 -> 0x86dd
  | Headers.Arp -> 0x806
  | Headers.Rarp -> 0x8035
  | Headers.Mpls -> 0x8847
  | _ -> abort_bpf_gen "unsupported protocol over gre"

let gre_proto (hdr : header) (pkt : packet) : sock_filter option =
  let base_idxs = get_base_idxs pkt Headers.Gre in
  let gre_proto_helper idxs =
    match idxs with
    | Ldx (Lit x) ->
      Some {
        code = [
          Ldb (Off (x+2));
        ];
        cond = Jeq (Hexj (gre_type hdr));
        jt = ret_true;
        jf = ret_false;
      }
    | List x ->
      Some {
        code = x @ [
          Ldh (Offset (Exp (X, Lit 2, Addition)));
        ];
        cond = Jeq (Hexj (gre_type hdr));
        jt = ret_true;
        jf = ret_false;
      }
    in
    gre_proto_helper base_idxs

let gre_to_sock_filter (gre_info : string list) (pkt : packet) : sock_filter option =
  match gre_info with
  | "proto" :: [protocol] -> gre_proto (string_to_header protocol) pkt