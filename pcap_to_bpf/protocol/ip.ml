(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: ip protocol
*)

open Inst
open Util
open Host
open Net
open Headers
open Protochain

let ipv4_type = function
  | Headers.Ip -> 0x0
  | Headers.Icmp -> 0x1
  | Headers.Tcp -> 0x6
  | Headers.Udp -> 0x11
  | Headers.Sctp -> 0x84
  | Headers.Gre -> 0x2f
  | _ -> failwith "Ip.ipv4_type"

let ipv4_frag (pkt : packet) x : sock_filter option =
  match (get_base_idxs pkt Headers.Ip) with
  | Ldx (Lit x) ->
    Some {
      code = [
        Ldh (Off (x+6));
      ];
      cond = Jset (Hexj 0x1fff);
      jt = ret_false;
      jf = ret_true;
    }
  | List x ->
    Some {
      code = x @ [
        Ldh (Offset (Exp (X, Lit 6, Addition)));
      ];
      cond = Jset (Hexj 0x1fff);
      jt = ret_false;
      jf = ret_true;
    }

let ip_ihl (pkt : packet) : opcode =
  Ldxb (Exp ((Lit 4), (Exp ((Off (sum_of_header pkt Headers.Ip)), (Hex 0xf), Arith_and)), Multiplication))

(* Old ip_proto function
let ip_proto (hdr : header) (pkt : packet) : sock_filter option =
  let sum_of_pred_headers = sum_of_header pkt Headers.Ip in
  Some {
    code = [
      Ldb (Off (sum_of_pred_headers + 9))
    ];
    cond = Jeq (Hexj (ipv4_type hdr));
    jt = ret_true;
    jf = ret_false;
  } *)

let ip_proto (hdr : header) (pkt : packet) : sock_filter option =
  match (get_base_idxs pkt Headers.Ip) with
  | Ldx (Lit x) ->
    Some {
      code = [
        Ldb (Off (x+9));
      ];
      cond = Jeq (Hexj (ipv4_type hdr));
      jt = ret_true;
      jf = ret_false;
    }
  | List x ->
    Some {
      code = x @ [
        Ldb (Offset (Exp (X, Lit 9, Addition)));
      ];
      cond = Jeq (Hexj (ipv4_type hdr));
      jt = ret_true;
      jf = ret_false;
    }

let ip_to_sock_filter (ip_info : string list) (pkt : packet) : sock_filter option =
  match ip_info with
  | "proto" :: [protocol] -> ip_proto (string_to_header protocol) pkt
  | "src" :: "and" :: "dst" :: "host" :: [host] -> conjoin (src_host Headers.Ip host pkt) (dst_host Headers.Ip host pkt)
  | "src" :: "or" :: "dst" :: "host" :: [host] -> disjoin (src_host Headers.Ip host pkt) (dst_host Headers.Ip host pkt)
  | "src" :: "host" :: [host] -> src_host Headers.Ip host pkt
  | "dst" :: "host" :: [host] -> dst_host Headers.Ip host pkt
  | "src" :: "and" :: "dst" :: "net" :: [net] -> conjoin (src_net Headers.Ip net pkt) (dst_net Headers.Ip net pkt)
  | "src" :: "or" :: "dst" :: "net" :: [net] -> disjoin (src_net Headers.Ip net pkt) (dst_net Headers.Ip net pkt)
  | "src" :: "net" :: [net] -> src_net Headers.Ip net pkt
  | "dst" :: "net" :: [net] -> dst_net Headers.Ip net pkt
  | "protochain" :: [protochain_num] ->
    Env.generate_protochain := true;
    Env.protochain_list := (int_of_string protochain_num :: !Env.protochain_list);
    protochain Headers.Ip protochain_num pkt
  | _ -> abort_bpf_gen "Ip.ip_to_sock_filter"
