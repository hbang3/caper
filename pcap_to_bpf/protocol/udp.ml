(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: udp
*)

open Port
open Portrange
open Inst
open Util
open Headers

let udp_type (hdr : header) : int =
  match hdr with
  | Headers.Vxlan -> 4789

let udp_proto (hdr : header) (pkt : packet) : sock_filter option = 
  let base_idx = get_base_idxs pkt Headers.Udp in
  match base_idx with
  | Ldx (Lit x) ->
    Some {
      code=[
        Ldh (Off (x + 2));
      ];
      cond= Jeq (Hexj (udp_type hdr));
      jt=ret_true;
      jf=ret_false;
    }
  | List x ->
    Some {
      code = x @ [
        Ldh (Offset (Exp (X, Lit 2, Addition)))
      ];
      cond= Jeq (Hexj (udp_type hdr));
      jt=ret_true;
      jf=ret_false;
    }

let udp_to_sock_filter (udp_info : string list) (pkt : packet) : sock_filter option =
  match udp_info with
  | "proto" :: [protocol] -> udp_proto (string_to_header protocol) pkt
  | "src" :: "and" :: "dst" :: "port" :: [port_num] -> src_dst_port "and" Headers.Udp port_num pkt
  | "src" :: "or" :: "dst" :: "port" :: [port_num] -> src_dst_port "or" Headers.Udp port_num pkt
  | "src" :: "port" :: [port_num] -> src_port Headers.Udp port_num pkt
  | "dst" :: "port" :: [port_num] -> dst_port Headers.Udp port_num pkt
  | "src" :: "and" :: "dst" :: "portrange" :: [portrange] -> src_dst_portrange "and" Headers.Udp portrange pkt
  | "src" :: "or" :: "dst" :: "portrange" :: [portrange] -> src_dst_portrange "or" Headers.Udp portrange pkt
  | "src" :: "portrange" :: [portrange] -> src_portrange Headers.Udp portrange pkt
  | "dst" :: "portrange" :: [portrange] -> dst_portrange Headers.Udp portrange pkt
  | _ -> abort_bpf_gen "Udp.udp_to_sock_filter"