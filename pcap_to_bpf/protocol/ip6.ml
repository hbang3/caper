(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: ip6 protocol
*)

open Inst
open Util
open Host
open Net
open Headers
open Protochain

let ipv6_type =  function
  | Headers.Ip -> 0x0
  | Headers.Icmp6 -> 0x3a
  | Headers.Tcp -> 0x6
  | Headers.Udp -> 0x11
  | Headers.Sctp -> 0x84
  | _ -> failwith "Ip6.ipv6_type"

let ip6_frag_proto (hdr : header) (pkt : packet) : sock_filter option =
  match (get_base_idxs pkt Headers.Ip6) with
  | Ldx (Lit x) ->
    let ipv6_frag1 = Some {
      code = [
        Ldb (Off (x+6));
      ];
      cond = Jeq (Hexj 0x2c);
      jt = ret_true;
      jf = ret_false;
    } in
    let ipv6_frag2 = Some {
      code = [
        Ldb (Off (x+40));
      ];
      cond = Jeq (Hexj (ipv6_type hdr));
      jt = ret_true;
      jf = ret_false;
    } in
    conjoin ipv6_frag1 ipv6_frag2
  | List x ->
    let ipv6_frag1 = Some {
      code = x @ [
        Ldb (Offset (Exp (X, Lit 6, Addition)));
      ];
      cond = Jeq (Hexj 0x2c);
      jt = ret_true;
      jf = ret_false;
    } in
    let ipv6_frag2 = Some {
      code = x @ [
        Ldb (Offset (Exp (X, Lit 40, Addition)));
      ];
      cond = Jeq (Hexj (ipv6_type hdr));
      jt = ret_true;
      jf = ret_false;
    } in
    conjoin ipv6_frag1 ipv6_frag2

let ip6_proto (hdr : header) (pkt : packet) : sock_filter option =
  match (get_base_idxs pkt Headers.Ip6) with
  | Ldx (Lit x) ->
    Some {
      code = [
        Ldb (Off (x+6));
      ];
      cond = Jeq (Hexj (ipv6_type hdr));
      jt = ret_true;
      jf = ret_false;
    }
  | List x ->
    Some {
      code = x @ [
        Ldb (Offset (Exp (X, Lit 6, Addition)));
      ];
      cond = Jeq (Hexj (ipv6_type hdr));
      jt = ret_true;
      jf = ret_false;
    }

let extend_ipv6 (ipv6 : string) =
  let parts = String.split_on_char ':' ipv6 in
  let full_parts, has_empty =
    List.fold_left (fun (acc, found_empty) part ->
      if part = "" then
        if found_empty then
          ("0000" :: acc, found_empty)
        else
          let fill = List.init (8 - List.length parts + 1) (fun _ -> "0000") in
          (fill @ acc, true)
      else (Printf.sprintf "%04x" (int_of_string ("0x" ^ part)) :: acc, found_empty)
    ) ([], false) parts
  in
  String.concat ":" (List.rev full_parts)

(*flag is true if ipv6 protocol still have furthre L4 to deal with*)
let ip6_to_sock_filter (ip6_info : string list) (frag : bool) (pkt : packet) : sock_filter option =
  match ip6_info with
  | "proto" :: [protocol] ->
    let ip6_proto_sf = ip6_proto (string_to_header protocol) pkt in
    if frag then disjoin ip6_proto_sf (ip6_frag_proto (string_to_header protocol) pkt)
    else ip6_proto_sf
  | "src" :: "and" :: "dst" :: "host" :: [host] -> conjoin (src_host Headers.Ip6 host pkt) (dst_host Headers.Ip6 host pkt)
  | "src" :: "or" :: "dst" :: "host" :: [host] -> disjoin (src_host Headers.Ip6 host pkt) (dst_host Headers.Ip6 host pkt)
  | "src" :: "and" :: "dst" :: "net" :: [net] -> conjoin (src_net Headers.Ip6 net pkt) (dst_net Headers.Ip6 net pkt)
  | "src" :: "or" :: "dst" :: "net" :: [net] -> disjoin (src_net Headers.Ip6 net pkt) (dst_net Headers.Ip6 net pkt)
  | "src" :: "host" :: [host] -> src_host Headers.Ip6 (extend_ipv6 host) pkt
  | "dst" :: "host" :: [host] -> dst_host Headers.Ip6 (extend_ipv6 host) pkt
  | "src" :: "net" :: [net] -> dst_host Headers.Ip6 net pkt
  | "dst" :: "net" :: [net] -> dst_host Headers.Ip6 net pkt
  | "protochain" :: [protochain_num] ->
    Env.generate_protochain := true;
    Env.protochain_list := (int_of_string protochain_num :: !Env.protochain_list);
    protochain Headers.Ip6 protochain_num pkt
  | _ -> abort_bpf_gen "unsupported ip6 feature"
