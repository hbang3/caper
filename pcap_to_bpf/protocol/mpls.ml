(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: mpls
*)

open Inst
open Headers
open Util

let is_mpls (pkt : packet) : sock_filter option =
  let l2_5 = get_l2_5 pkt in
  let l2_5_len = count_headers l2_5 in
  Some {
    code = [Ldh (Off (14 + l2_5_len * 4 - 6))];
    cond = Jeq (Hexj 0x8847);
    jt = ret_true;
    jf = ret_false;
  }

let is_bottom (pkt : packet) : sock_filter option =
  let l2_5 = get_l2_5 pkt in
  let l2_5_len = count_headers l2_5 in
  Some {
    code = [Ldb (Off (14 + l2_5_len * 4 - 6))];
    cond = Jset (Hexj 0x1);
    jt = ret_true;
    jf = ret_false;
  }

let rec mpls_val_shift (mpls_val : int) : int =
  Int.shift_left mpls_val 12

let mpls_to_sock_filter (mpls_info : string list) (pkt : packet) : sock_filter option =
  let l2_5 = get_l2_5 pkt in
  let l2_5_len = count_headers l2_5 in
  match mpls_info, l2_5 with
  | [], Packet ((Mpls, _), Nil) -> is_mpls pkt
  | [], Packet ((Mpls, _), Packet ((Mpls, _), _)) -> is_bottom pkt
  | [mpls_val], Packet((Mpls,_), Packet((Mpls, _), _)) ->
    let _mpls_val = mpls_val_shift (int_of_string mpls_val) in
    let sf = Some {code = [
      Ld (Off (14 + l2_5_len * 4 - 4));
      And (Hexj 0xfffff000)];
      cond = Jeq (Hexj (_mpls_val));
      jt = ret_true;
      jf = ret_false
    }in
    conjoin (negate (is_bottom pkt)) sf
  | [mpls_val], Packet ((Mpls, _), _) | [mpls_val], Packet ((Mpls, _), Packet ((Vlan, _), _))->
    let _mpls_val = mpls_val_shift (int_of_string mpls_val) in
    let sf = Some {
      code = [
        Ld (Off (14 + l2_5_len * 4 - 4));
        And (Hexj 0xfffff000)
      ];
      cond = Jeq (Hexj (_mpls_val));
      jt = ret_true;
      jf = ret_false
    } in
    conjoin (is_mpls pkt) sf
