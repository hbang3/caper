(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: tcp
*)

open Inst
open Headers
open Port
open Portrange
open Util

let is_tcp_flag  = function
| "tcp-fin"
| "tcp-syn"
| "tcp-rst"
| "tcp-push"
| "tcp-ack"
| "tcp-urg" -> true
| _ -> false

let tcp_flag_to_opcode = function
| "tcp-fin" -> Num 0x01
| "tcp-syn" -> Num 0x02
| "tcp-rst" -> Num 0x04
| "tcp-push" -> Num 0x08
| "tcp-ack" -> Num 0x10
| "tcp-urg" -> Num 0x20
| _ -> abort_bpf_gen "Tcp.tcp_flag_to_opcode"

let tcp_to_sock_filter (tcp_info : string list) (pkt : packet) : sock_filter option =
  match tcp_info with
  | "src" :: "and" :: "dst" :: "port" :: [port_num] -> src_dst_port "and" Headers.Tcp port_num pkt
  | "src" :: "or" :: "dst" :: "port" :: [port_num] -> src_dst_port "or" Headers.Tcp port_num pkt
  | "src" :: "port" :: [port_num] -> src_port Headers.Tcp port_num pkt
  | "dst" :: "port" :: [port_num] -> dst_port Headers.Tcp port_num pkt
  | "src" :: "and" :: "dst" :: "portrange" :: [portrange] -> src_dst_portrange "and" Headers.Tcp portrange pkt
  | "src" :: "or" :: "dst" :: "portrange" :: [portrange] -> src_dst_portrange "or" Headers.Tcp portrange pkt
  | "src" :: "portrange" :: [portrange] -> src_portrange Headers.Tcp portrange pkt
  | "dst" :: "portrange" :: [portrange] -> dst_portrange Headers.Tcp portrange pkt
  | _ -> abort_bpf_gen "Tcp.tcp_to_sock_filter"