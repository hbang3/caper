(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: arp protocol
*)


open Inst
open Util
open Host
open Net
open Headers

let arp_to_sock_filter (arp_info : string list) (pkt : packet) : sock_filter option =
  match arp_info with
  | "src" :: "and" :: "dst" :: "host" :: [host] -> conjoin (src_host Headers.Arp host pkt) (dst_host Headers.Arp host pkt)
  | "src" :: "or" :: "dst" :: "host" :: [host] -> disjoin (src_host Headers.Arp host pkt) (dst_host Headers.Arp host pkt)
  | "src" :: "host" :: [host] -> src_host Headers.Arp host pkt
  | "dst" :: "host" :: [host] -> dst_host Headers.Arp host pkt
  | "src" :: "and" :: "dst" :: "net" :: [net] -> conjoin (src_net Headers.Arp net pkt) (dst_net Headers.Arp net pkt)
  | "src" :: "or" :: "dst" :: "net" :: [net] -> disjoin (src_net Headers.Arp net pkt) (dst_net Headers.Arp net pkt)
  | "src" :: "net" :: [net] -> src_net Headers.Arp net pkt
  | "dst" :: "net" :: [net] -> dst_net Headers.Arp net pkt
  | _ -> abort_bpf_gen "unsupported ip feature"
