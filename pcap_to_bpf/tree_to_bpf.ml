(*
  Copyright Hyunsuk Bang, December 2022

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: sock_filter tree to BPF
*)

open Inst
open Block
open Util
open Str
open Eval
open Bpf_optimizer

let line_num = ref 0
let accept_packet l =  Printf.sprintf "l%03d: %s" l (pretty_format "ret" ("#" ^ string_of_int !Config.snap_len))
let drop_packet l = Printf.sprintf "l%03d: %s" (l+1) (pretty_format "ret" "#0")

let acpt_packet_block l = {key=l; opcodes=[Ret True]; cond = No_cond; jt = (-1); jf = (-1)}
let drop_packet_block l = {key=l; opcodes=[Ret False]; cond = No_cond; jt = (-1); jf = (-1)}

let append_ret blocks = blocks @ [(acpt_packet_block !line_num);(drop_packet_block (!line_num + 1))]

let jt_jf_to_int = function
  | Some {code = [Ret True]; cond = True_cond; jt = None; jf = None;} -> (-2)
  | Some {code = [Ret False]; cond = False_cond; jt = None; jf = None;} -> (-1)
  | _ -> failwith "Tree_to_bpf.jt_jf_to_int"

let rec flatten (sf : sock_filter option) : block list =
  match (Option.get sf).code with
  | [Ret True] -> []
  | [Ret False] -> []
  | [Logical_And] -> flatten (Option.get sf).jt @ flatten (Option.get sf).jf @ [{key=(-1); opcodes=[Logical_And]; cond=No_cond; jt=(-1); jf=(-1)}]
  | [Logical_Or] -> flatten (Option.get sf).jt @ flatten (Option.get sf).jf @ [{key=(-1); opcodes=[Logical_Or]; cond=No_cond; jt=(-1); jf=(-1)}]
  | other ->
    line_num := !line_num + List.length other + 1;
    let cond = (Option.get sf).cond in
    let jt = (Option.get sf).jt in
    let jf = (Option.get sf).jf in
    [{key=(!line_num); opcodes=other; cond=cond; jt=jt_jf_to_int jt; jf=jt_jf_to_int jf}]

let rec parse_line_num block =
  match block with
  | {key=(-1); opcodes=[Logical_And]; cond=No_cond; jt=(-1); jf=(-1)}
  | {key=(-1); opcodes=[Logical_Or]; cond=No_cond; jt=(-1); jf=(-1)} -> block
  | {key=line; opcodes=opcodes; cond=cond; jt=jt; jf=jf} ->
    let l = (line - !line_num) * (-1) in
    {key=l; opcodes=opcodes; cond=cond; jt=jt; jf=jf}

let rec process stack blocks =
  let eval_logical_and (b1 : block list) (b2 : block list) =
    let jmp_line = (List.hd b2).key in
    let parse_true = function
      | {key=l; opcodes=opcodes; cond=c; jt=(-2); jf=jf;} -> {key=l; opcodes=opcodes; cond=c; jt=jmp_line; jf=jf;}
      | {key=l; opcodes=opcodes; cond=c; jt=jt; jf=(-2);}  -> {key=l; opcodes=opcodes; cond=c; jt=jt; jf=jmp_line;}
      | other -> other in
    (List.map parse_true b1) @ b2
  in
  let eval_logical_or (b1 : block list) (b2 : block list) =
    let jmp_line = (List.hd b2).key in
    let parse_false = function
      | {key=l; opcodes=opcodes; cond=c; jt=(-1); jf=jf;} -> {key=l; opcodes=opcodes; cond=c; jt=jmp_line; jf=jf;}
      | {key=l; opcodes=opcodes; cond=c; jt=jt; jf=(-1);}  -> {key=l; opcodes=opcodes; cond=c; jt=jt; jf=jmp_line;}
      | other -> other in
    (List.map parse_false b1) @ b2
  in
  match blocks with
  | [] -> List.hd stack
  | [{key=_; opcodes=[Logical_And]; cond=_; jt=_; jf=_}] :: rest ->
    let b1 = List.hd (List.tl stack) in
    let b2 = List.hd stack in
    process (eval_logical_and b1 b2 :: (List.tl (List.tl stack))) rest
  | [{key=_; opcodes=[Logical_Or]; cond=_; jt=_; jf=_}] :: rest->
    let b1 = List.hd (List.tl stack) in
    let b2 = List.hd stack in
    process (eval_logical_or b1 b2 :: (List.tl (List.tl stack))) rest
  | other :: rest -> process (other :: stack) rest

let parse_jt_jf (b : block) =
  let map_jt_jf = function
    | (-2) -> !line_num
    | (-1) -> !line_num + 1
    | other -> other
  in
  {key = b.key; opcodes = b.opcodes; cond = b.cond; jt = map_jt_jf b.jt; jf = map_jt_jf b.jf;}

let sock_filter_tree_to_bpf (sf : sock_filter option) =
  line_num := 0;
  let preoptimized_bpf =
    sf
    |> flatten
    |> List.map parse_line_num
    |> List.map (fun a -> [a])
    |> process []
    |> List.map parse_jt_jf
    |> append_ret
  in
  let bpf =
    if !Config.generate_optimized_bpf_output then
      optimize preoptimized_bpf
    else
      preoptimized_bpf
  in
  bpf
