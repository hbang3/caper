(*
  Copyright Hyunsuk Bang, February 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Arithmetic and byte_offset evaluation
*)

open Inst
open Headers
open Util
open Ether
open Ip
open Ip6
open Tcp
open Udp
open Sctp
open Icmp
open Icmp6
open Infix_to_postfix

let mem_idx = ref (-1)

let eval_expr_size (opcode_stack : opcode list) (pkt : packet): sock_filter option =
  Some {
    code = op_list_to_list (List.hd opcode_stack);
    cond = True_cond;
    jt = ret_true;
    jf = ret_false;
  }

let eval (operator : string) (op1 : opcode) (op2 : opcode) (pkt : packet) : opcode =
  let eval_num_num (operator : string) (num_op1 : opcode) (num_op2 : opcode) : opcode =
    let op1_int = num_to_int num_op1 in
    let op2_int = num_to_int num_op2 in
    match operator with
    | "+" -> Num (op1_int + op2_int)
    | "-" -> Num (op1_int - op2_int)
    | "*" -> Num (op1_int * op2_int)
    | "/" -> Num (op1_int / op2_int)
    | "%" -> Num (op1_int mod op2_int)
    | "&" -> Num (op1_int land op2_int)
    | "|" -> Num (op1_int lor op2_int)
    | "<<" -> Num (Int.shift_left op1_int op2_int)
    | ">>" -> Num (Int.shift_right op1_int op2_int)
    | _ -> failwith "Eval.eval_num_num"
  in
  let eval_proto_num (operator : string) (proto_op : opcode) (num_op : opcode) (pkt : packet) : opcode =
    let eval op op2_int =
      match op with
      | "+" -> [Add (Hexj (op2_int))]
      | "-" -> [Sub (Hexj (op2_int))]
      | "*" -> [Mul (Hexj (op2_int))]
      | "/" -> [Div (Hexj (op2_int))]
      | "%" -> [Mod (Hexj (op2_int))]
      | "&" -> [And (Hexj (op2_int))]
      | "|" -> [Or (Hexj (op2_int))]
      | "<<" -> [Lsh (Hexj (op2_int))]
      | ">>" -> [Rsh (Hexj (op2_int))]
      | _ -> failwith "Eval.eval_proto_num" in
    List (op_list_to_list proto_op @ (eval operator (num_to_int num_op)))
  in
  let eval_num_proto (operator : string) (num_op : opcode) (proto_op : opcode) (pkt : packet) : opcode =
    mem_idx := !mem_idx + 1;
    let op = function
      | "+" -> Add X
      | "-" -> Sub X
      | "*" -> Mul X
      | "/" -> Div X
      | "%" -> Mod X
      | "&" -> And X
      | "|" -> Or X
      | "<<" -> Lsh X
      | ">>" -> Rsh X in
    match operator, num_op, proto_op with
    | "+", _, _ ->  eval_proto_num operator proto_op num_op pkt
    | "*", _, _ ->  eval_proto_num operator proto_op num_op pkt
    | other, Num n, List x -> List (x @ [Tax; Ld (Lit n); op other])
    | other, Num n, x ->  List ([x; Tax; Ld (Lit n); op other])
  in
  let eval_proto_proto (operator : string) (proto_op1 : opcode) (proto_op2 : opcode) (pkt : packet) : opcode =
    mem_idx := !mem_idx + 1;
    let op = function
      | "+" -> Add X
      | "-" -> Sub X
      | "*" -> Mul X
      | "/" -> Div X
      | "%" -> Mod X
      | "&" -> And X
      | "|" -> Or X
      | "<<" -> Lsh X
      | ">>" -> Rsh X in
    match proto_op1, proto_op2 with
    | List x, List y -> List (x @ (St (Mem !mem_idx) :: y) @ [Tax; Ld (Mem !mem_idx); op operator])
    | List x, y -> List (x @ [St (Mem !mem_idx); y; Tax; Ld (Mem !mem_idx); op operator])
    | x, List y -> List ([x; St (Mem !mem_idx)] @ y @ [Tax; Ld (Mem !mem_idx); op operator])
    | x, y ->  List ([x; St (Mem !mem_idx); y; Tax; Ld (Mem !mem_idx); op operator])
  in
  match op1, op2 with
  | Num n1, Num n2 -> eval_num_num operator op1 op2
  | Num n, op -> eval_num_proto operator op1 op2 pkt
  | op, Num n -> eval_proto_num operator op1 op2 pkt
  | op1, op2 -> eval_proto_proto operator op1 op2 pkt

(* comparing values *)
let comp (comparison : string) (opcode_stack : opcode list) (pkt : packet) : sock_filter option =
  let comp_int_int (comparison : string) (num1 : opcode) (num2 : opcode) : sock_filter option =
    let n1 = num_to_int num1 in
    let n2 = num_to_int num2 in
    match comparison with
    | "=" | "==" -> if n1 = n2 then ret_true else ret_false
    | "!=" -> if n1 != n2 then ret_true else ret_false
    | "<" -> if n1 < n2 then ret_true else ret_false
    | ">" -> if n1 > n2 then ret_true else ret_false
    | "<=" -> if n1 <= n2 then ret_true else ret_false
    | ">=" -> if n1 >= n2  then ret_true else ret_false
    | _ -> failwith "Eval.comp_int_int"
  in
  let comp_opcode_int (comparison : string) (op : opcode) (num : opcode) (pkt : packet) : sock_filter option =
    let comp =
      let n = num_to_int num in
      match comparison with
      | "=" | "==" -> Some {code = (op_list_to_list op); cond = Jeq (Hexj n); jt = ret_true; jf = ret_false}
      | "!=" -> Some {code = (op_list_to_list op); cond = Jeq (Hexj n); jt = ret_false; jf = ret_true}
      | "<" -> Some {code = (op_list_to_list op); cond = Jge (Hexj n); jt = ret_false; jf = ret_true}
      | ">" -> Some {code = (op_list_to_list op); cond = Jgt (Hexj n); jt = ret_true; jf = ret_false}
      | "<=" -> Some {code = (op_list_to_list op); cond = Jgt (Hexj n); jt = ret_false; jf = ret_true}
      | ">=" -> Some {code = (op_list_to_list op); cond = Jge (Hexj n); jt = ret_true; jf = ret_false}
      | _ -> failwith "Eval.comp_opcode_int" in
    match get_l3 pkt with
    | Packet ((Ip, _), _) -> conjoin (ipv4_frag pkt Headers.Ip (* doesnot matter which layer4 protocol *)) comp
    | _ -> comp
  in
  let comp_int_opcode (comparison : string) (num : opcode) (op : opcode) (pkt : packet) : sock_filter option =
    match comparison with
    | "=" | "==" | "!=" -> comp_opcode_int comparison op num pkt
    | "<" -> comp_opcode_int ">" op num pkt
    | ">" -> comp_opcode_int "<" op num pkt
    | ">=" -> comp_opcode_int "<=" op num pkt
    | "<=" -> comp_opcode_int ">=" op num pkt
  in
  let comp_opcode_opcode (comparison : string) (op1 : opcode) (op2 : opcode) (pkt : packet) : sock_filter option =
    let comp =
      let op = (op_list_to_list op1) @ [St (Mem 1)] @ (op_list_to_list op2) @ [Tax; Ld (Mem 1)] in
      match comparison with
      | "=" | "==" -> (Some {code = op; cond = Jeq X; jt = ret_true; jf = ret_false});
      | "!=" -> (Some {code = op; cond = Jeq X; jt = ret_false; jf = ret_true});
      | "<" -> (Some {code = op; cond = Jge X; jt = ret_false; jf = ret_true});
      | ">" -> (Some {code = op; cond = Jgt X; jt = ret_true; jf = ret_false});
      | "<=" -> (Some {code = op; cond = Jgt X; jt = ret_false; jf = ret_true});
      | ">=" -> (Some {code = op; cond = Jge X; jt = ret_true; jf = ret_false});
      | _ -> failwith "Eval.comp_opcode_opcode" in
    match get_l3 pkt with
    | Packet ((Ip, _), _) -> conjoin (ipv4_frag pkt Headers.Ip (* doesnot matter which layer4 protocol *)) comp
    | _ -> comp
  in
  match opcode_stack with
  | Num n2 :: Num n1 :: [] -> comp_int_int comparison (Num n1) (Num n2)
  | Num n :: op :: [] -> comp_opcode_int comparison op (Num n) pkt
  | op :: Num n :: [] -> comp_int_opcode comparison (Num n) op pkt
  | op2 :: op1 :: [] -> comp_opcode_opcode comparison op1 op2 pkt
  | _ -> failwith "Eval.comp"

let rec eval_pcaps (opcode_stack : opcode list) (pcaps : string list) (pkt : packet) : sock_filter option =
  match pcaps with
  | [] -> eval_expr_size opcode_stack pkt
  | "=" :: [] | "==" :: [] | "!=" :: [] | "<" :: [] | ">" :: [] | "<=" :: [] | ">=" :: [] -> comp (List.hd pcaps) opcode_stack pkt
  | "+" :: rest | "-" :: rest | "*" :: rest | "/" :: rest | "%" :: rest
  | "&" :: rest | "|" :: rest | ">>" :: rest | "<<" :: rest ->
    let op2 = List.hd opcode_stack in
    let op1 = List.hd (List.tl opcode_stack) in
    eval_pcaps (eval (List.hd pcaps) op1 op2 pkt :: (List.tl (List.tl opcode_stack))) rest pkt
  | head :: rest ->
    if is_array head then eval_pcaps (proto_array_to_sock_filter head pkt :: opcode_stack) rest pkt else
    if is_digit head || is_hex head then eval_pcaps ((Num (int_of_string head)) :: opcode_stack) rest pkt else
    if is_icmp_type head then eval_pcaps (icmp_type_to_opcode head :: opcode_stack) rest pkt else
    if is_icmp6_type head then eval_pcaps (icmp6_type_to_opcode head :: opcode_stack) rest pkt else
    if is_tcp_flag head then eval_pcaps (tcp_flag_to_opcode head :: opcode_stack) rest pkt
    else failwith "Eval.eval_pcaps"
  | _ -> failwith "Eval.eval_pcaps"

and l2_expr_size (hdr : header) (expr : string) (size : int) (pkt : packet) : opcode =
  match get_l2 pkt with
  | Packet ((l2, _), _) ->
    if l2 <> hdr then failwith "Eval.l2_expr_size" else
    if is_digit expr then
      let inst =
        match size with
        | 1 -> Ldb (Offset (Lit (int_of_string expr)))
        | 2 -> Ldh (Offset (Lit (int_of_string expr)))
        | 4 -> Ld (Offset (Lit (int_of_string expr)))
        | _ -> failwith "Eval.l2_expr_size ( 1, 2 ,4 )"
      in
      List (inst :: [])
    else
      let expr_op = (Option.get (eval_pcaps [] (pcap_infix_to_postfix expr) pkt)).code in
      let inst =
        match size with
        | 1 -> Ldb (Offset (Exp (X, (Lit (0)), Addition)))
        | 2 -> Ldh (Offset (Exp (X, (Lit (0)), Addition)))
        | 4 -> Ld (Offset (Exp (X, (Lit (0)), Addition)))
        | _ -> failwith "Eval.l2_expr_size ( 1, 2, 4 )"
      in
      List (expr_op @ (Tax :: inst :: []))

and l3_expr_size (hdr : header) (expr : string) (size : int) (pkt : packet) : opcode =
  match get_l3 pkt with
  | Packet ((l3, _), _) ->
    if l3 <> hdr then failwith "Eval.l3_expr_size" else
      let pred_header_size = sum_of_header pkt hdr in
      if is_digit expr then
        let inst =
          match size with
          | 1 -> Ldb (Offset (Lit (pred_header_size + int_of_string expr)))
          | 2 -> Ldh (Offset (Lit (pred_header_size + int_of_string expr)))
          | 4 -> Ld (Offset (Lit (pred_header_size + int_of_string expr)))
          | _ -> failwith "Eval.l3_expr_size"
        in
        List (inst :: [])
      else
        let expr_op = (Option.get (eval_pcaps [] (pcap_infix_to_postfix expr) pkt)).code in
        let inst =
          match size with
          | 1 -> Ldb (Offset (Exp (X, (Lit (pred_header_size)), Addition)))
          | 2 -> Ldh (Offset (Exp (X, (Lit (pred_header_size)), Addition)))
          | 4 -> Ld (Offset (Exp (X, (Lit (pred_header_size)), Addition)))
          | _ -> failwith "Eval.l3_expr_size"
        in
        List (expr_op @ (Tax :: inst :: []))

and l4_expr_size (hdr : header) (expr : string) (size : int) (pkt : packet) : opcode =
  match get_l3 pkt with
  | Packet ((Ip, _), _) ->
    if (peek (get_l4 pkt)) <> hdr then failwith "Eval.l4_expr_size" else
      let pred_header_size = sum_of_header pkt Headers.Ip in
      if is_digit expr then
        let inst =
          match size with
          | 1 -> Ldb (Offset (Exp (X, (Lit (pred_header_size + int_of_string expr)), Addition)))
          | 2 -> Ldh (Offset (Exp (X, (Lit (pred_header_size + int_of_string expr)), Addition)))
          | 4 -> Ld (Offset (Exp (X, (Lit (pred_header_size + int_of_string expr)), Addition)))
          | _ -> failwith "Eval.l4_expr_size"
        in
        List (ip_ihl pkt :: inst :: [])
      else
        let expr_op = (Option.get (eval_pcaps [] (pcap_infix_to_postfix expr) pkt)).code in
        let inst =
          match size with
          | 1 -> Ldb (Offset (Exp (X, (Lit (pred_header_size)), Addition)))
          | 2 -> Ldh (Offset (Exp (X, (Lit (pred_header_size)), Addition)))
          | 4 -> Ld (Offset (Exp (X, (Lit (pred_header_size)), Addition)))
          | _ -> failwith "Eval.l4_expr_size"
        in
        List (expr_op @ (ip_ihl pkt :: (Add X) :: Tax :: inst :: []))
  | Packet ((Ip6, _), _) ->
    if (peek (get_l4 pkt)) <> hdr then failwith "l4_expr_size" else
    if is_digit expr then
      match size with
      | 1 -> Ldb (Off (int_of_string expr + sum_of_header pkt hdr))
      | 2 -> Ldh (Off (int_of_string expr + sum_of_header pkt hdr))
      | 4 -> Ld (Off (int_of_string expr + sum_of_header pkt hdr))
      | _ -> failwith "Eval.l4_expr_size"
    else
      let expr_op = (Option.get (eval_pcaps [] (pcap_infix_to_postfix expr) pkt)).code in
      let inst =
        match size with
        | 1 -> Ldb (Offset (Exp (X, (Lit (sum_of_header pkt hdr)), Addition)))
        | 2 -> Ldh (Offset (Exp (X, (Lit (sum_of_header pkt hdr)), Addition)))
        | 4 -> Ld (Offset (Exp (X, (Lit (sum_of_header pkt hdr)), Addition)))
        | _ -> failwith "Eval.l4_expr_size"
      in
      List (expr_op @ (Tax :: inst :: []))

and l2_offset (hdr : header) (idx : string) (pkt : packet) : opcode =
  match pkt with
  | Packet ((l2, _), _) ->
    if l2 <> hdr then failwith (Printf.sprintf "get %s, Expected: %s" (header_to_string hdr) (header_to_string l2))  else
    if is_digit idx then
      Ldb (Off (int_of_string idx))
    else
      let idx_op = (Option.get (eval_pcaps [] (pcap_infix_to_postfix idx) pkt)).code in
      List (idx_op @ (Tax :: Ldb (Offset (Exp (X, (Lit (0)), Addition))) :: []))

and l3_offset (hdr : header) (idx : string) (pkt : packet) : opcode =
  match get_l3 pkt with
  | Packet ((l3, _), _) ->
    if l3 <> hdr then failwith "Eval.l3_offset" else
    if is_digit idx then
      Ldb (Off (sum_of_header pkt hdr + int_of_string idx))
    else
      let idx_op = (Option.get (eval_pcaps [] (pcap_infix_to_postfix idx) pkt)).code in
      List (idx_op @ (Tax :: Ldb (Offset (Exp (X, (Lit (sum_of_header pkt hdr)), Addition))) :: []))

and l4_offset (hdr : header) (idx : string) (pkt : packet) : opcode =
  match (get_l3 pkt) with
  | Packet ((Ip, _), _) ->
    let pred_header_size = sum_of_header pkt Headers.Ip in
    if (peek (get_l4 pkt)) <> hdr then failwith "Eval.l4_offset" else
    if is_digit idx then
      List ([ip_ihl pkt; Ldb (Offset (Exp (X, (Lit (int_of_string idx + pred_header_size)), Addition)))])
    else
      let idx_op = (Option.get (eval_pcaps [] (pcap_infix_to_postfix idx) pkt)).code in
      List (idx_op @ (Ldxb (Exp ((Lit 4), (Exp ((Off (pred_header_size)), (Hex 0xf), Arith_and)), Multiplication )) :: Add X :: Tax :: Ldb (Offset (Exp (X, (Lit (pred_header_size)), Addition))) :: []))
  | Packet ((Ip6, _), _) ->
    if (peek (get_l4 pkt)) <> hdr then failwith "Eval.l4_offset" else
    if is_digit idx then
      Ldb (Off (sum_of_header pkt hdr + int_of_string idx))
    else
      let idx_op = (Option.get (eval_pcaps [] (pcap_infix_to_postfix idx) pkt)).code in
      List (idx_op @ (Tax :: Ldb (Offset (Exp (X, Lit (sum_of_header pkt hdr), Addition))) :: []))

(* offset : protocol[offset] *)
and proto_offset (hdr : header) (offset : string) (pkt : packet) : opcode =
  match hdr with
  | Headers.Ether ->
    l2_offset hdr offset (encapsulate hdr pkt)
  | Headers.Arp
  | Headers.Rarp
  | Headers.Ip
  | Headers.Ip6 ->
    l3_offset hdr offset (encapsulate hdr pkt)
  | Headers.Icmp
  | Headers.Icmp6
  | Headers.Tcp
  | Headers.Udp
  | Headers.Sctp ->
    l4_offset hdr offset (encapsulate hdr pkt)
  | _ -> failwith "Eval.proto_offset"

and proto_expr_size (hdr : header) (expr_size : string) (pkt : packet) : opcode =
  let _expr_size = List.rev (String.split_on_char ':' expr_size) in
  let size =
    _expr_size
    |> List.hd
    |> String.trim
    |> int_of_string
  in
  let expr =
    _expr_size
    |> List.tl
    |> List.rev
    |> String.concat ":"
    |> String.trim
  in
  match hdr with
  | Headers.Ether ->
    l2_expr_size hdr expr size (encapsulate hdr pkt)
  | Headers.Arp
  | Headers.Rarp
  | Headers.Ip
  | Headers.Ip6 ->
    l3_expr_size hdr expr size (encapsulate hdr pkt)
  | Headers.Icmp
  | Headers.Icmp6
  | Headers.Tcp
  | Headers.Udp
  | Headers.Sctp ->
    l4_expr_size hdr expr size (encapsulate hdr pkt)
  | _ -> failwith "Eval.proto_expr_size"

and proto_array_to_sock_filter (proto_array: string) (pkt : packet) : opcode =
  match String.split_on_char '[' proto_array with
  | _proto :: rest ->
    let proto = string_to_header _proto in
    let offset = String.concat "[" rest in
    let _offset = String.sub offset 0 (String.length offset - 1) in
    if is_expr_size _offset then proto_expr_size proto _offset pkt else
    if _offset = "icmptype" then proto_offset proto "0" pkt else
    if _offset = "icmp6type" then proto_offset proto "0" pkt else
    if _offset = "icmpcode" then proto_offset proto "1" pkt else
    if _offset = "icmp6code" then proto_offset proto "1" pkt else
    if _offset = "tcpflags" then proto_offset proto "13" pkt
    else proto_offset proto _offset pkt
  | _ -> failwith "Eval.proto_array_to_sock_filter"
