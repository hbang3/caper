(*
  Copyright Hyunsuk Bang, March 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Block type definition
*)

open Inst
open Util

type block = {
  key: int;
  opcodes: opcode list;
  cond: cond;
  jt: int;
  jf: int;
}

let replace_opcodes (new_opcodes : opcode list) (b : block) : block =
  match b with
  | {key= l; opcodes=ops; cond=c; jt = jt; jf = jf;} ->
    {key= l; opcodes=new_opcodes; cond=c; jt = jt; jf = jf;}

let replace_jt (new_jt : int) (b : block) : block =
  match b with
  | {key = k; opcodes = ops; cond = c; jt = jt; jf = jf;} ->
    {key = k; opcodes = ops; cond = c; jt = new_jt; jf = jf;}

let replace_jf (new_jf : int) (b : block) : block =
  match b with
  | {key = k; opcodes = ops; cond = c; jt = jt; jf = jf;} ->
    {key = k; opcodes = ops; cond = c; jt = jt; jf = new_jf;}

let shift_block (offset : int) (b : block) : block =
  match b with
  | {key=key; opcodes=opcodes; cond=cond; jt=jt; jf=jf;} ->
    {key=key+offset; opcodes=opcodes; cond=cond; jt=jt+offset; jf=jf+offset;}

let get_blocks_length (blocks : block list) =
  let rec get_blocks_length_helper (ret : int) (blocks : block list) =
  match blocks with
  | [] -> ret
  | b :: bs ->
    match b with
    | {key=line; opcodes=[Ret True]; cond = No_cond; jt = _; jf = _}
    | {key=line; opcodes=[Ret False]; cond = No_cond; jt = _; jf = _} -> get_blocks_length_helper ret bs
    | {key=line; opcodes=opcodes; cond=c; jt=jt; jf=jf} -> get_blocks_length_helper (ret + List.length opcodes + 1) bs
  in
  get_blocks_length_helper 0 blocks

let assign_new_jt_jf old_jt old_jf new_jt new_jf block =
  match block with
  | {key=line; opcodes=opcodes; cond=c; jt=jt; jf=jf} ->
    if jt = old_jt && jf = old_jf then
      {key=line; opcodes=opcodes; cond=c; jt=new_jt; jf=new_jf}
    else if jt =  old_jt then
      {key=line; opcodes=opcodes; cond=c; jt=new_jt; jf=jf}
    else if jf = old_jf then
      {key=line; opcodes=opcodes; cond=c; jt=jt; jf=new_jf}
    else
      block

let assign_new_jump_line old_jump new_jump block =
  match block with
  | {key=line; opcodes=opcodes; cond=c; jt=jt; jf=jf} ->
    if jt = old_jump then
      {key=line; opcodes=opcodes; cond=c; jt=new_jump; jf=jf}
    else if jf = old_jump then
      {key=line; opcodes=opcodes; cond=c; jt=jt; jf=new_jump}
    else
      block

let conditional_shift_jump_line cond (offset : int) block =
  match block with
  | {key=line; opcodes=[]; cond = Jmp (Lit n); jt = jt; jf = jf;} ->
    if cond n then
      {key=line; opcodes=[]; cond = Jmp (Lit (n + offset)); jt = jt; jf = jf;}
    else
      block
  | {key=line; opcodes=opcodes; cond=c; jt=jt; jf=jf} ->
    if cond jt && cond jf then
      {key=line; opcodes=opcodes; cond=c; jt=jt + offset; jf=jf + offset}
    else if cond jt then
      {key=line; opcodes=opcodes; cond=c; jt=jt + offset; jf=jf}
    else if cond jf then
      {key=line; opcodes=opcodes; cond=c; jt=jt; jf=jf + offset}
    else
      block

let disjoin_two_blocks (blocks1 : block list) (blocks2 : block list) =
  let shift_offset = get_blocks_length blocks1 in
  let new_blocks_jt_line = (get_blocks_length blocks1) + (get_blocks_length blocks2) in
  let _blocks2 = List.map (shift_block shift_offset) blocks2 in
  let _blocks1 =
    blocks1
    |> lst_rev
    |> List.tl
    |> List.tl
    |> lst_rev
    |> lst_map (assign_new_jump_line (shift_offset) (new_blocks_jt_line))
    |> lst_map (assign_new_jump_line (shift_offset + 1) (shift_offset))
  in
  lst_append _blocks1 _blocks2

let insert_protochain_blocks (main_blocks : block list) protochains icmp6_protochains protochain_bridges =
  let rec insert_protochain_blocks_helper (ret : block list) (main_blocks : block list) =
  match main_blocks with
  | [] -> lst_rev ret
  | b :: bs ->
    let protochain_block_opt =
      begin
        match b with
        | {key=line; opcodes=_; cond = Ip_Protochain_Bridge_cond (Lit n); jt = jt; jf = jf;}
        | {key=line; opcodes=_; cond = Ip6_Protochain_Bridge_cond (Lit n); jt = jt; jf = jf;}
        | {key=line; opcodes=_; cond = Icmp6_Protochain_Bridge_cond (Lit n); jt = jt; jf = jf;} ->
          List.assoc_opt n protochain_bridges
        | {key=line; opcodes=_; cond = Ip_Protochain_cond (Lit n); jt = jt; jf = jf;}
        | {key=line; opcodes=_; cond = Ip6_Protochain_cond (Lit n); jt = jt; jf = jf;} ->
          List.assoc_opt n protochains
        | {key=line; opcodes=_; cond = Icmp6_Protochain_cond (Lit n); jt = jt; jf = jf;} ->
          List.assoc_opt n icmp6_protochains
        | _ -> None
      end
    in
    let protochain_length =
      match protochain_block_opt with
      | Some x -> get_blocks_length x
      | None -> 0
    in
    match protochain_block_opt with
    | Some protochain_block ->
      let new_ret = lst_map (conditional_shift_jump_line (( < ) b.key) (protochain_length - 1)) ret in
      let old_jt = b.key+ protochain_length in
      let old_jf = b.key+ protochain_length + 1 in
      let new_jt = b.jt + protochain_length - 1 in
      let new_jf = b.jf + protochain_length - 1 in
      let shifted_protochain_block =
        protochain_block
        |> lst_rev
        |> lst_map (shift_block b.key)
        |> lst_map (assign_new_jt_jf old_jt old_jf new_jt new_jf)
        |> List.tl
        |> List.tl
      in
      let new_bs = lst_map (shift_block (protochain_length - 1)) bs in
      let new_blocks = lst_append (lst_rev (lst_append shifted_protochain_block new_ret)) new_bs in
      insert_protochain_blocks_helper [] new_blocks
    | None -> insert_protochain_blocks_helper (b :: ret) bs
  in
  insert_protochain_blocks_helper [] main_blocks

(*
  function insert_ja_blocks is to prevent bit overflow on jt and jf parts of BPF.

  struct sock_filter {          /* Filter block */
	__u16	code;           /* Actual filter code */
	__u8	jt;	        /* Jump true */
	__u8	jf;	        /* Jump false */
	__u32	k;              /* Generic multiuse field */
  };

  jt and jf can only go up to 255.
  If jump offset is bigger than 255, this will generate BPF with wrong semnatics due to the bit overflow.
  create new jmp block and connect overflowing jt and jf to that jmp block can prevent the overflow.
*)
let rec insert_jmp_blocks (blocks : block list) : block list =
  let rec insert_jmp_blocks_helper (ret : block list) (blocks : block list) : block list =
    match blocks with
    | [] -> lst_rev ret
    | b :: bs ->
      let jt_offset = b.jt - b.key - 1 in
      let jf_offset = b.jf - b.key - 1 in
      if jt_offset > 255 && jf_offset > 255 then
        let new_ret = lst_map (conditional_shift_jump_line (( < ) b.key) 2) ret in
        let new_jt_ja = {
          key = b.key + (List.length b.opcodes) + 1;
          opcodes = [];
          cond = Jmp (Lit (b.jt + 2));
          jt = (-1);
          jf = (-1);
        } in
        let new_jf_ja = {
          key = b.key + (List.length b.opcodes) + 2;
          opcodes = [];
          cond = Jmp (Lit (b.jf + 2));
          jt = (-1);
          jf = (-1);
        } in
        let new_b = {
          key = b.key;
          opcodes = b.opcodes;
          cond = b.cond;
          jt = new_jt_ja.key;
          jf = new_jf_ja.key;
        } in
        insert_jmp_blocks_helper (new_jt_ja :: new_jf_ja :: new_b :: new_ret) (lst_map (shift_block 2) bs)
      else if jt_offset > 255 then
        let new_ret = lst_map (conditional_shift_jump_line (( < ) b.key) 1) ret in
        let new_jt_ja = {
          key = b.key + (List.length b.opcodes) + 1;
          opcodes = [];
          cond = Jmp (Lit (b.jt + 1));
          jt = (-1);
          jf = (-1);
        } in
        let new_b = {
          key = b.key;
          opcodes = b.opcodes;
          cond = b.cond;
          jt = new_jt_ja.key;
          jf = b.jf + 1;
        } in
        insert_jmp_blocks_helper (new_jt_ja :: new_b :: new_ret) (lst_map (shift_block 1) bs)
      else if jf_offset > 255 then
        let new_ret = lst_map (conditional_shift_jump_line (( < ) b.key) 1) ret in
        let new_jf_ja = {
          key = b.key + (List.length b.opcodes) + 1;
          opcodes = [];
          cond = Jmp (Lit (b.jf + 1));
          jt = (-1);
          jf = (-1);
        } in
        let new_b = {
          key = b.key;
          opcodes = b.opcodes;
          cond = b.cond;
          jt = b.jt + 1;
          jf = new_jf_ja.key;
        } in
        insert_jmp_blocks_helper (new_jf_ja :: new_b :: new_ret) (lst_map (shift_block 1) bs)
      else
        insert_jmp_blocks_helper (b :: ret) bs
  in
  insert_jmp_blocks_helper [] blocks

let blocks_to_graph (blocks : block list) : string =
  let rec block_to_label (block : block) : string =
    match block with
    | {key=line; opcodes=[Ret True]; cond = No_cond; jt = _; jf = _} ->
      Printf.sprintf  "(%03d) ret #%d" line !Config.snap_len
    | {key=line; opcodes=[Ret False]; cond = No_cond; jt = _; jf = _} ->
      Printf.sprintf "(%03d) ret #0" line
    | {key=line; opcodes=[]; cond=Ja (Lit n); jt=jt; jf=jf} ->
      Printf.sprintf "(%03d) ja (%03d)" line n
    | {key=line; opcodes=[]; cond=c; jt=jt; jf=jf} ->
      Printf.sprintf "(%03d) %-23s  jt %-5d  jf %d" (line) (cond_to_str c) jt jf
    | {key=line; opcodes=opcodes; cond=c; jt=jt; jf=jf} ->
      (Printf.sprintf "(%03d) %-23s\\l" line (opcode_to_str (List.hd opcodes))) ^ block_to_label {key=line + 1; opcodes=List.tl opcodes; cond=c; jt=jt; jf=jf}
  in
  let rec draw_nodes (ret : string list) (blocks : block list) =
    match blocks with
    | [] -> (String.concat "\n" (lst_rev ret))
    | acpt_pkt :: drop_pkt :: [] ->
      let acpt_pkt_node = Printf.sprintf "%d [label=\"%s\" constraint=false];" acpt_pkt.key (block_to_label acpt_pkt) in
      let drop_pkt_node = Printf.sprintf "%d [label=\"%s\" constraint=false];" drop_pkt.key (block_to_label drop_pkt) in
      let rank = Printf.sprintf "{rank=max; %d; %d;}" acpt_pkt.key drop_pkt.key in
      draw_nodes (rank :: acpt_pkt_node :: drop_pkt_node :: ret) []
    | b :: bs ->
      let graph = Printf.sprintf "%d [shape=box; label=\"\\l %s\"];" b.key (block_to_label b) in
      draw_nodes (graph :: ret) bs
  in
  let rec draw_edges (ret: string list) (blocks: block list) =
    match blocks with
    | [] -> String.concat "\n" (lst_rev ret)
    | acpt_pkt :: drop_pkt :: [] ->
      let pos = Printf.sprintf "%d -> %d[style=invis];" acpt_pkt.key drop_pkt.key in
      draw_edges (pos :: ret) []
    | b :: bs ->
      let graph = Printf.sprintf "%d -> %d [label=\"T\"];\n%d -> %d [label=\"F\"];" b.key b.jt b.key b.jf in
      draw_edges (graph :: ret) bs
  in
  let nodes = draw_nodes [] blocks in
  let edges = draw_edges [] blocks in
  Printf.sprintf "digraph g {\n%s\n%s\n}" nodes edges

let blocks_to_bpf (blocks : block list) : string =
  let rec block_to_bpf (block : block) : string =
    if !Config.parse_and_prettyprint_mode then
      begin
        match block with
        | {key=line; opcodes=[Ret True]; cond = No_cond; jt = _; jf = _} ->
          Printf.sprintf "(%03d) ret #%d" line !Config.snap_len
        | {key=line; opcodes=[Ret False]; cond = No_cond; jt = _; jf = _} ->
            Printf.sprintf "(%03d) ret #0" line
        | {key=line; opcodes=[]; cond=Jmp (Lit n); jt=jt; jf=jf} ->
            Printf.sprintf "(%03d) jmp (%03d)" line n
        | {key=line; opcodes=[]; cond=c; jt=jt; jf=jf} ->
          Printf.sprintf "(%03d) %-23s  jt %-5d  jf %d" (line) (cond_to_str c) jt jf
        | {key=line; opcodes=opcodes; cond=c; jt=jt; jf=jf} ->
          (Printf.sprintf "(%03d) %-23s\n" line (opcode_to_str (List.hd opcodes))) ^ block_to_bpf {key=line + 1; opcodes=List.tl opcodes; cond=c; jt=jt; jf=jf}
      end
    else
      begin
        match block with
        | {key=line; opcodes=[Ret True]; cond = No_cond; jt = _; jf = _} ->
          Printf.sprintf  "l%03d: ret #%d" line !Config.snap_len
        | {key=line; opcodes=[Ret False]; cond = No_cond; jt = _; jf = _} ->
            Printf.sprintf  "l%03d: ret #0" line
        | {key=line; opcodes=[]; cond=Jmp (Lit n); jt=jt; jf=jf} ->
            Printf.sprintf "l%03d: jmp l%03d" line n
        | {key=line; opcodes=[]; cond=c; jt=jt; jf=jf} ->
          Printf.sprintf  "l%03d: %s , l%03d , l%03d" (line) (cond_to_str c) jt jf
        | {key=line; opcodes=opcodes; cond=c; jt=jt; jf=jf} ->
          (Printf.sprintf "l%03d: %s\n" line (opcode_to_str (List.hd opcodes))) ^ block_to_bpf {key=line + 1; opcodes=List.tl opcodes; cond=c; jt=jt; jf=jf}
      end
  in
  String.concat "\n" (List.map block_to_bpf blocks)

let blocks_to_output (blocks : block list) : string =
  if !Config.generate_bpf_graph_output then
    blocks_to_graph blocks
  else
    blocks_to_bpf blocks
