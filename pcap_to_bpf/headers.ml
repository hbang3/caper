(*
  Copyright Hyunsuk Bang, February 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Header type definition for context
*)

open Inst

type header =
  | Ether
  | Mpls
  | Vlan
  | Ip
  | Ip6
  | Arp
  | Rarp
  | Icmp
  | Icmp6
  | Tcp
  | Udp
  | Sctp
  | Gre
  | Vxlan

type packet =
  | Nil
  | Packet of (header * opcode) * packet

let peek (pkt : packet) =
  match pkt with
  | Nil -> failwith "peek from Nil"
  | Packet ((hdr, _), hdrs) -> hdr

let string_to_header = function
  | "ether" -> Ether
  | "vlan" -> Vlan
  | "mpls" -> Mpls
  | "ip" -> Ip
  | "ip6" -> Ip6
  | "icmp" -> Icmp
  | "icmp6" -> Icmp6
  | "arp" -> Arp
  | "rarp" -> Rarp
  | "tcp" -> Tcp
  | "udp" -> Udp
  | "sctp" -> Sctp
  | "gre" -> Gre
  | "vxlan" -> Vxlan
  | _ -> failwith "unsupported Layer"

let header_to_string = function
  | Ether -> "ether"
  | Vlan -> "vlan"
  | Mpls -> "mpls"
  | Ip -> "ip"
  | Ip6 -> "ip6"
  | Arp -> "arp"
  | Rarp -> "rarp"
  | Icmp -> "icmp"
  | Icmp6 -> "icmp6" | Tcp -> "tcp" | Udp -> "udp"
  | Sctp -> "sctp"
  | Gre -> "gre"
  | Vxlan -> "vxlan"
  | _ -> failwith "unsupported Layer"


let get_hdr_size = function
  | Ether -> 14
  | Vlan -> 4
  | Mpls -> 4
  | Ip6 -> 40
  | Arp -> 28
  | Rarp -> 28
  | Icmp -> 8
  | Icmp6 -> 8
  | Tcp -> 20 (* tcp can have variable length*)
  | Udp -> 8
  | Vxlan -> 22 (* vxlan + new ether *)
  (* | Sctp -> "sctp"
  | Gre -> "gre" *)
  | _ -> failwith "unsupported Layer"

let encapsulate (hdr : header) (pkt : packet) =
  let calc_base_idx opcode hdr =
    match opcode, hdr with
    | List x, Ip ->
      List (x @ [
        Txa;
        Ldxb (Exp (Lit 4, (Exp (Offset X, Hex 0xf, Arith_and)), Multiplication));
        Add X;
        Tax;
      ])
    | List x, Gre ->
      List (x @ [
        Ldb (Offset (Exp (X, Lit 0, Addition)));
        Rsh (Hexj 7);
        Mul (Hexj 8);
        Add X;
        Add (Hexj 4);
        Tax;
      ])
    | Ldx (Lit x), Ip ->
      List [
        Ldx (Hexj x);
        Txa;
        Ldxb (Exp (Lit 4, (Exp (Off x, Hex 0xf, Arith_and)), Multiplication));
        Add X;
        Tax
      ]
    | Ldx (Lit x), Gre ->
      List [
        Ldx (Hexj x);
        Ldb (Off x);
        Rsh (Hexj 7);
        Mul (Hexj 8);
        Add X;
        Add (Hexj 4);
        Tax;
      ]
    | List x, _ ->
      List (x @ [
        Ld (Hexj (get_hdr_size hdr));
        Add X;
        Tax;
      ])
    | Ldx (Lit x), _ -> Ldx (Lit (x + (get_hdr_size hdr)))
  in
  let rec encapsulate_helper (hdr : header) (pkt : packet) =
    match pkt with
    | Nil -> Packet ((hdr, Ldx (Lit 0)), Nil)
    | Packet ((fst_hdr, fst_opcode), Nil) ->
      Packet (
        (fst_hdr, fst_opcode),
        Packet (
          (hdr, calc_base_idx fst_opcode fst_hdr),
          Nil
        )
      )
    | Packet ((fst_hdr, fst_opcode), rest_pkt) ->
      Packet (
        (fst_hdr, fst_opcode),
        encapsulate_helper hdr rest_pkt
      )
  in
  match pkt, hdr with
  | Nil, Mpls | Nil, Vlan -> Packet ((Ether, Ldx (Lit 0)), Packet ((hdr, Ldx (Lit 14)), Nil))
  | Nil, Ether -> Packet ((Ether, Ldx (Lit 0)), Nil)
  | Packet ((Ether, _), _), Ether -> pkt
  | Packet (h, Nil), _ | Packet (h, _), _ -> encapsulate_helper hdr pkt

let get_l2 (pkt : packet) =
  match pkt with
  | Nil -> Nil
  | Packet ((Ether, x), hdrs) -> Packet ((Ether, x), Nil)
  | Packet (hdr, hdrs) -> Nil

let get_l2_5 (pkt : packet) =
  let rec get_vlans (pkt : packet) (base_pkt : packet) =
    match pkt with
    | Packet ((Vlan, _), hdrs) -> get_vlans hdrs (encapsulate Vlan base_pkt)
    | _ -> base_pkt
  in
  let rec get_mpls (pkt : packet) (base_pkt : packet) =
    match pkt with
    | Packet ((Mpls, _), hdrs) -> get_mpls hdrs (encapsulate Mpls base_pkt)
    | _ -> base_pkt
  in
  let rec get_l2_5_helper (pkt: packet) =
    match pkt with
    | Packet ((Vlan, _), hdrs) -> get_vlans hdrs (Packet ((Vlan, Ldx (Lit 14)), Nil))
    | Packet ((Mpls, _), hdrs) -> get_mpls hdrs (Packet ((Mpls, Ldx (Lit 14)), Nil))
    | Packet (hdr, hdrs) -> get_l2_5_helper hdrs
    | Nil -> Nil
  in
  get_l2_5_helper pkt

let rec get_l3 (pkt : packet) =
  match pkt with
  | Nil -> Nil
  | Packet ((Ip, x), hdrs) -> Packet ((Ip, x), Nil)
  | Packet ((Ip6, x), hdrs) -> Packet ((Ip6, x), Nil)
  | Packet ((Arp, x), hdrs) -> Packet ((Arp, x), Nil)
  | Packet ((Rarp, x), hdrs) -> Packet ((Rarp, x) , Nil)
  | Packet (hdr, hdrs) -> get_l3 hdrs

let rec get_l4 (pkt : packet) =
  match pkt with
  | Nil -> Nil
  | Packet ((Tcp, x), hdrs) -> Packet ((Tcp,x) , Nil)
  | Packet ((Udp, x), hdrs) -> Packet ((Udp,x), Nil)
  | Packet ((Icmp, x), hdrs) -> Packet ((Icmp,x), Nil)
  | Packet ((Icmp6, x), hdrs) -> Packet ((Icmp6,x), Nil)
  | Packet (hdr, hdrs) -> get_l4 hdrs

let count_headers (pkt : packet) =
  let rec count_headers_helper (ret : int) (pkt : packet) =
    match pkt with
    | Nil -> ret
    | Packet (hdr, hdrs) -> count_headers_helper (ret + 1) hdrs
  in
  count_headers_helper 0 pkt

let sum_of_header (pkt : packet) (target : header) =
  let rec sum_of_header_helper (pkt : packet) (acc : int) =
    if (peek pkt) == target then
      acc
    else
      match pkt with
      | Nil -> acc
      | Packet ((Ether, _) , hdrs) -> sum_of_header_helper hdrs (acc + 14)
      | Packet ((Vlan, _), hdrs)   -> sum_of_header_helper hdrs (acc + 4)
      | Packet ((Mpls, _), hdrs)   -> sum_of_header_helper hdrs (acc + 4)
      | Packet ((Ip6, _), hdrs)    -> sum_of_header_helper hdrs (acc + 40)
      | Packet ((Udp, _), hdrs)    -> sum_of_header_helper hdrs (acc + 8)
  in
  sum_of_header_helper pkt 0

let get_base_idxs (pkt : packet) (target : header) =
  let rec get_base_idxs_helper (pkt : packet) (ret : opcode) =
    match pkt with
    | Nil -> ret
    | Packet ((hdr, hdr_opcode), rest_hdr) ->
      if hdr == target then
        get_base_idxs_helper rest_hdr hdr_opcode
      else
        get_base_idxs_helper rest_hdr ret
  in
  get_base_idxs_helper pkt Nil_op